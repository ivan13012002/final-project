﻿"""This is a player interface for python."""
# -*- coding: utf-8 -*-
##
#    \file
#    \brief This is a player interface for python.
#
#    Interface of the AVСP (C++) class for Python using ctypes.
#    Supports the "Windows" operating system starting with "Windows 7".
##

import ctypes
import os
from pathlib import Path

DIR_OF_LIB = './AVCPLib.so'  # Путь до файла библиотеки


##
#    Structure with information about track.
##
class AudioStreamInfo(ctypes.Structure):
    """Structure with information about track."""
    _fields_ = [
        ('title', ctypes.c_char_p),  # < Track title
        ('path', ctypes.c_char_p),  # < Track path
        ('playlist', ctypes.c_char_p),  # < The playlist that the track belongs to
        ('channel_count', ctypes.c_int),  # < Channel count of track
        ('sample_rate', ctypes.c_int),  # < Sample rate of track
        ('block_size', ctypes.c_int),  # < Block size of track (in bits)
        ('volume', ctypes.c_float),  # < The volume of the audio stream
        ('is_playing', ctypes.c_bool),  # < Does the track play
        ('is_paused', ctypes.c_bool),  # < Is the track on pause
        ('is_processing', ctypes.c_bool)]


##
#    \brief AVCP class interface.
#    \author Melnikov Matvey
#    \version 0.1
#    \date May 7, 2020
#
#    Creates an AVCP (C++) class interface for Python using ctypes.
##
class AVCPClass:
    """AVCP class interface."""

    ##
    #    Constructor.
    #    \param[in] dir The path to the tracks. For example: D:\\\music\\\\.
    ##
    def __init__(self, dir=None):
        """Constructor."""
        ###Constructor and destructor
        self.avcpmodule = ctypes.cdll.LoadLibrary(DIR_OF_LIB)
        self.avcpmodule.getNew.restype = ctypes.c_void_p

        self.avcpmodule.deleteClass.argtypes = [ctypes.c_void_p]
        self.thisclass = self.avcpmodule.getNew()

        ###Methods

        self.avcpmodule.AVCP_getPath.argtypes = [ctypes.c_void_p]
        self.avcpmodule.AVCP_getPath.restype = ctypes.c_char_p

        self.avcpmodule.AVCP_setPath.argtypes = [ctypes.c_void_p, ctypes.c_char_p]

        self.avcpmodule.AVCP_play.argtypes = [ctypes.c_void_p]

        self.avcpmodule.AVCP_pause.argtypes = [ctypes.c_void_p]

        self.avcpmodule.AVCP_stop.argtypes = [ctypes.c_void_p]

        self.avcpmodule.AVCP_playNext.argtypes = [ctypes.c_void_p]

        self.avcpmodule.AVCP_playPrevious.argtypes = [ctypes.c_void_p]

        self.avcpmodule.AVCP_getTrackTime.argtypes = [ctypes.c_void_p]
        self.avcpmodule.AVCP_getTrackTime.restype = ctypes.c_float

        self.avcpmodule.AVCP_getTrackTimeNow.argtypes = [ctypes.c_void_p]
        self.avcpmodule.AVCP_getTrackTimeNow.restype = ctypes.c_float

        self.avcpmodule.AVCP_setTrackTimeNow.argtypes = [ctypes.c_void_p, ctypes.c_int]

        self.avcpmodule.AVCP_getQueue.argtypes = [ctypes.c_void_p, ctypes.POINTER(ctypes.c_int)]
        self.avcpmodule.AVCP_getQueue.restype = ctypes.POINTER(ctypes.c_char_p)

        self.avcpmodule.AVCP_getTrackInfo.argtypes = [ctypes.c_void_p]
        self.avcpmodule.AVCP_getTrackInfo.restype = AudioStreamInfo
        ###
        self.avcpmodule.AVCP_startPlaylist.argtypes = [ctypes.c_void_p, ctypes.c_char_p]

        self.avcpmodule.AVCP_createPlaylist.argtypes = [ctypes.c_void_p, ctypes.c_char_p]

        self.avcpmodule.AVCP_deletePlaylist.argtypes = [ctypes.c_void_p, ctypes.c_char_p]

        self.avcpmodule.AVCP_renamePlaylist.argtypes = [
            ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]

        self.avcpmodule.AVCP_addTrackToPlaylist.argtypes = [
            ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]

        self.avcpmodule.AVCP_deleteTrackFromPlaylist.argtypes = [
            ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]
        ####
        self.avcpmodule.AVCP_loadTracksFromFile.argtypes = [ctypes.c_void_p]

        self.avcpmodule.AVCP_savePlaylists.argtypes = [ctypes.c_void_p]

        self.avcpmodule.AVCP_findPlaylist.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        self.avcpmodule.AVCP_findPlaylist.restype = ctypes.c_bool

        self.avcpmodule.AVCP_getPlaylists.argtypes = [ctypes.c_void_p, ctypes.POINTER(ctypes.c_int)]
        self.avcpmodule.AVCP_getPlaylists.restype = ctypes.POINTER(ctypes.c_char_p)

        self.avcpmodule.AVCP_findTrack.argtypes = [
            ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]
        self.avcpmodule.AVCP_findTrack.restype = ctypes.c_bool

        self.avcpmodule.AVCP_getTracks.argtypes = [
            ctypes.c_void_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_int)]
        self.avcpmodule.AVCP_getTracks.restype = ctypes.POINTER(ctypes.c_char_p)

        if dir != None:
            self.set_path(dir)
        else:
            path = os.getcwd()
            self.set_path(path)

    def __del__(self):
        """Destructor."""
        self.avcpmodule.deleteClass(self.thisclass)

    ##
    #    Returns the path to the folder with tracks.
    ##
    def get_path(self):
        """Returns the path to the folder with tracks."""
        return self.avcpmodule.AVCP_getPath(self.thisclass).decode("utf-8")

    ##
    #    Sets the path to the folder with tracks.
    #    \param[in] dir The path to the tracks. For example: D:\\\music\\\.
    ##

    def set_path(self, dir):
        """Sets the path to the folder with tracks."""
        self.avcpmodule.AVCP_setPath(self.thisclass, str(dir).encode("utf-8"))

    ##
    #    Starts playing tracks.
    ##

    def play(self):
        """Starts playing tracks."""
        self.avcpmodule.AVCP_play(self.thisclass)

    ##
    #    Stop playing tracks.
    ##

    def pause(self):
        """Stop playing tracks."""
        self.avcpmodule.AVCP_pause(self.thisclass)

    ##
    #    Stop player.
    ##

    def stop(self):
        """Stop player."""
        self.avcpmodule.AVCP_stop(self.thisclass)

    ##
    #    Start playing next track.
    ##

    def play_next(self):
        """Start playing next track."""
        self.avcpmodule.AVCP_playNext(self.thisclass)

    ##
    #    Start playing previous track.
    ##

    def play_previous(self):
        """Start playing previous track."""
        self.avcpmodule.AVCP_playPrevious(self.thisclass)

    ##
    #    Returns the total track time in seconds.
    ##

    def get_time(self):
        """Returns the total track time in seconds."""
        return self.avcpmodule.AVCP_getTrackTime(self.thisclass)

    ##
    #    Returns the total track time in minutes and seconds.
    ##

    def get_time_dict(self):
        """Returns the total track time in minutes and seconds."""
        return {'Minutes': int(self.get_time() / 60),
                'Seconds': int(self.get_time() - (int(self.get_time() / 60) * 60))}

    ##
    #    Returns the current track time in seconds.
    ##

    def get_time_now(self):
        """Returns the current track time in seconds."""
        return self.avcpmodule.AVCP_getTrackTimeNow(self.thisclass)

    ##
    #    Sets the position of the track.
    ##

    def set_time_now(self, seconds):
        """Sets the position of the track."""
        self.avcpmodule.AVCP_setTrackTimeNow(self.thisclass, seconds)

    ##
    #    Returns the current track time in minutes and seconds.
    ##

    def get_time_now_dict(self):
        """Returns the current track time in minutes and seconds."""
        return {'Minutes': int(self.get_time_now() / 60),
                'Seconds': int(self.get_time_now() - (int(self.get_time_now() / 60) * 60))}

    ##
    #    Returns the remaining time in seconds.
    ##

    def get_remaining_time(self):
        """Returns the remaining time in seconds."""
        return self.get_time() - self.get_time_now()

    ##
    #    Returns the remaining time in minutes and seconds.
    ##

    def get_remaining_time_dict(self):
        """Returns the remaining time in minutes and seconds."""
        remaining_time = self.get_time() - self.get_time_now()
        return {'Minutes': int(remaining_time / 60),
                'Seconds': int(remaining_time - (int(remaining_time / 60) * 60))}

    ##
    #    Returns the queue of tracks.
    ##

    def get_queue(self):
        """Returns the queue of tracks."""
        queue_size = ctypes.c_int()
        cqueue = self.avcpmodule.AVCP_getQueue(self.thisclass, ctypes.byref(queue_size))
        queue = []
        for i in range(queue_size.value):
            queue.append(cqueue[i].decode("utf-8"))
        return queue

    ##
    #    Returns the structure with information about track.
    ##

    def get_track_info(self):
        """Returns the structure with information about track."""
        info = self.avcpmodule.AVCP_getTrackInfo(self.thisclass)
        return {'title': info.title.decode("utf-8"), 'path': info.path.decode("utf-8"),
                'playlist': info.playlist.decode("utf-8"), 'channel_count': info.channel_count,
                'sample_rate': info.sample_rate, 'block_size': info.block_size,
                'volume': info.volume, 'is_playing': info.is_playing,
                'is_paused': info.is_paused, 'is_processing': info.is_processing}

    ##
    #    Plays a playlist.
    ##

    def start_playlist(self, playlist_name):
        """Plays a playlist."""
        self.avcpmodule.AVCP_startPlaylist(self.thisclass, str(playlist_name).encode("utf-8"))

    ##
    #    Creates an empty playlist and saves it to a file (name of file: "playlists").
    ##

    def create_playlist(self, playlist_name):
        """Creates an empty playlist and saves it to a file (name of file: "playlists")."""
        self.avcpmodule.AVCP_createPlaylist(self.thisclass, str(playlist_name).encode("utf-8"))

    ##
    #    Deletes a playlist from a file (name of file: "playlists").
    ##

    def delete_playlist(self, playlist_name):
        """Deletes a playlist from a file (name of file: "playlists")."""
        self.avcpmodule.AVCP_deletePlaylist(self.thisclass, str(playlist_name).encode("utf-8"))

    ##
    #    Renames the playlist and saves it to a file (name of file: "playlists").
    ##

    def rename_playlist(self, new_playlist_name, playlist_name):
        """Renames the playlist and saves it to a file (name of file: "playlists")."""
        self.avcpmodule.AVCP_renamePlaylist(
            self.thisclass, str(new_playlist_name).encode("utf-8"),
            str(playlist_name).encode("utf-8"))

    ##
    #    Adds a track to the playlist.
    ##

    def add_track_to_playlist(self, track_path, playlist_name):
        """Adds a track to the playlist."""
        self.avcpmodule.AVCP_addTrackToPlaylist(
            self.thisclass, str(track_path).encode("utf-8"), str(playlist_name).encode("utf-8"))

    ##
    #    Deletes a track from the playlist.
    ##

    def delete_track_from_playlist(self, track_name, playlist_name):
        """Deletes a track from the playlist."""
        self.avcpmodule.AVCP_deleteTrackFromPlaylist(
            self.thisclass, str(track_name).encode("utf-8"), str(playlist_name).encode("utf-8"))

    ##
    #    Loads tracks and playlists from a file (name of file: "playlists").
    ##

    def load_tracks_from_file(self):
        """Loads tracks and playlists from a file (name of file: "playlists")."""
        self.avcpmodule.AVCP_loadTracksFromFile(self.thisclass)

    ##
    #    Saves tracks and playlists to a file (name of file: "playlists").
    ##

    def save_playlists(self):
        """Saves tracks and playlists to a file (name of file: "playlists")."""
        self.avcpmodule.AVCP_savePlaylists(self.thisclass)

    ##
    #    Checks whether the playlist exists.
    ##

    def find_playlist(self, playlist_name):
        """Checks whether the playlist exists."""
        return self.avcpmodule.AVCP_findPlaylist(self.thisclass, str(playlist_name).encode("utf-8"))

    ##
    #    Returns a list of all saved playlists.
    ##

    def get_playlists(self):
        """Returns a list of all saved playlists."""
        playlists_size = ctypes.c_int()
        cplaylists = self.avcpmodule.AVCP_getPlaylists(self.thisclass, ctypes.byref(playlists_size))
        playlists = []
        for i in range(playlists_size.value):
            playlists.append(cplaylists[i].decode("utf-8"))
        return playlists

    ##
    #    Checks whether the track exists.
    ##

    def find_track(self, track_name, playlist_name):
        """Checks whether the track exists."""
        return self.avcpmodule.AVCP_findTrack(
            self.thisclass, str(track_name).encode("utf-8"), str(playlist_name).encode("utf-8"))

    ##
    #    Returns a list of tracks in the playlist.
    ##

    def get_tracks(self, playlist_name):
        """Returns a list of tracks in the playlist."""
        tracks_size = ctypes.c_int()
        ctracks = self.avcpmodule.AVCP_getTracks(
            self.thisclass, str(playlist_name).encode("utf-8"), ctypes.byref(tracks_size))
        tracks = []
        for i in range(tracks_size.value):
            tracks.append(Path(ctracks[i].decode("utf-8")).stem)
        return tracks

﻿"""This is a player interface for python."""
# -*- coding: utf-8 -*-
##
#    \file
#    \brief This is a avc class interface for python.
#
#    Interface of the AVС (C++) class for Python using ctypes.
#    Supports the "Windows" operating system starting with "Windows 7".
##

import ctypes

DIR_OF_LIB = './AVCLib.so'  # Путь до файла библиотеки


##
#    \brief AVC class interface.
#    \author Melnikov Matvey
#    \version 0.2
#    \date May 19, 2020
#
#    Creates an AVC (C++) class interface for Python using ctypes.
##
class AVCClass:
    """AVC class interface."""

    ##
    #    Constructor.
    #    \param[in] default_vol The volume to be set. For example: 0.2 (20%).
    #    \param[in] range The range to be set. For example: 0.2 (20%).
    ##
    def __init__(self, _default_vol=0.3, _range=0.1):
        """Constructor."""
        self.avcmodule = ctypes.cdll.LoadLibrary(DIR_OF_LIB)

        self.avcmodule.GetNew.restype = ctypes.c_void_p

        self.avcmodule.DeleteClass.argtypes = [ctypes.c_void_p]  # Деструктор

        self.thisclass = self.avcmodule.GetNew()

        # --------Методы

        self.avcmodule.AVC_startVolumeControl.argtypes = [ctypes.c_void_p]

        self.avcmodule.AVC_stopVolumeControl.argtypes = [ctypes.c_void_p]

        self.avcmodule.AVC_stopVolumeControl.argtypes = [ctypes.c_void_p]

        self.avcmodule.AVC_isWorking.argtypes = [ctypes.c_void_p]
        self.avcmodule.AVC_isWorking.restype = ctypes.c_bool

        self.avcmodule.AVC_getMasterVolume.argtypes = [ctypes.c_void_p]
        self.avcmodule.AVC_getMasterVolume.restype = ctypes.c_float

        self.avcmodule.AVC_getDefaultVolume.argtypes = [ctypes.c_void_p]
        self.avcmodule.AVC_getDefaultVolume.restype = ctypes.c_float

        self.avcmodule.AVC_getVolume.argtypes = [ctypes.c_void_p]
        self.avcmodule.AVC_getDefaultVolume.restype = ctypes.c_float

        self.avcmodule.AVC_setDefaultVolume.argtypes = [ctypes.c_void_p, ctypes.c_float]

        self.avcmodule.AVC_getRange.argtypes = [ctypes.c_void_p]
        self.avcmodule.AVC_getRange.restype = ctypes.c_float

        self.avcmodule.AVC_setRange.argtypes = [ctypes.c_void_p, ctypes.c_float]

        self.avcmodule.AVC_saveSpecifiedVolume.argtypes = [
            ctypes.c_void_p, ctypes.c_char_p, ctypes.c_float]

        self.avcmodule.AVC_deleteSpecifiedVolume.argtypes = [ctypes.c_void_p, ctypes.c_int]

        self.avcmodule.AVC_clearSavedVolumes.argtypes = [ctypes.c_void_p]

        self.avcmodule.AVC_getSpecifiedVolumeApps.argtypes = [
            ctypes.c_void_p, ctypes.POINTER(ctypes.c_int)]
        self.avcmodule.AVC_getSpecifiedVolumeApps.restype = ctypes.POINTER(ctypes.c_char_p)

        self.avcmodule.AVC_getLatestApps.argtypes = [ctypes.c_void_p, ctypes.POINTER(ctypes.c_int)]
        self.avcmodule.AVC_getLatestApps.restype = ctypes.POINTER(ctypes.c_char_p)

        self.set_default_volume(_default_vol)
        self.set_range(_range)

    def __del__(self):
        """Destructor."""
        self.avcmodule.DeleteClass(self.thisclass)

    ##
    #    Starts volume control of the sound system.
    ##
    def start_volume_control(self):
        """Starts volume control."""
        self.avcmodule.AVC_startVolumeControl(self.thisclass)

    ##
    #    Stops volume control of the sound system.
    ##
    def stop_volume_control(self):
        """Stops volume control."""
        self.avcmodule.AVC_stopVolumeControl(self.thisclass)

    ##
    #    Checks whether sound control is working.
    ##
    def is_working(self):
        """Checks whether sound control is working."""
        return self.avcmodule.AVC_isWorking(self.thisclass)

    ##
    #    Method for finding out the volume of the system mixer.
    ##
    def get_master_volume(self):
        """Method for finding out the volume of the system mixer."""
        return float("{0:.2f}".format(self.avcmodule.AVC_getMasterVolume(self.thisclass)))

    ##
    #    Returns the volume value that is set while the app is running,
    #    but the volume value is not saved for it.
    ##
    def get_default_volume(self):
        """Returns the default volume value"""
        return float("{0:.2f}".format(self.avcmodule.AVC_getDefaultVolume(self.thisclass)))

    ##
    #    Sets the default volume.
    #    \param[in] _default_volume The volume to be set. For example: 0.2 (20%).
    ##
    def set_default_volume(self, default_vol):
        """Sets the default volume."""
        self.avcmodule.AVC_setDefaultVolume(self.thisclass, default_vol)

    ##
    #    Returns the range of volume.
    ##
    def get_range(self):
        """ Returns the range of volume."""
        return float("{0:.2f}".format(self.avcmodule.AVC_getRange(self.thisclass)))

    ##
    #    Sets the range of volume.
    #    \param[in] _range The range to be set. For example: 0.2 (20%).
    ##
    def set_range(self, range):
        """Sets the range of volume."""
        self.avcmodule.AVC_setRange(self.thisclass, range)

    ##
    #    Saves the app and the volume for it in a file (name of file: "info").
    #    \param[in] app_name Name of the app that will be saved. For example: any_app.exe.
    #    \param[in] _volume The volume that will be saved for this app. For example: 0.2 (20%).
    ##
    def save_specified_volume(self, programm_name, volume):
        """Saves the app and the volume for it in a file."""
        self.avcmodule.AVC_saveSpecifiedVolume(
            self.thisclass, str(programm_name).encode("utf-8"), volume)

    ##
    #    Deletes the saved volume value for the app by position.
    #    \param[in] pos_of_program The position of app, which will be deleted.
    ##
    def delete_specified_volume(self, pos_of_program):
        """Deletes the saved volume value for the app."""
        self.avcmodule.AVC_deleteSpecifiedVolume(self.thisclass, pos_of_program)

    ##
    #    Deletes all saved volume values.
    ##
    def clear_saved_volumes(self):
        """Deletes all saved volume values."""
        self.avcmodule.AVC_clearSavedVolumes(self.thisclass)

    ##
    #    Returns a list of programs with saved volume values from a file (name of file: "info").
    ##
    def get_specified_volume_apps(self):
        """Returns a list of programs with saved volume values."""
        apps_list_size = ctypes.c_int()
        capps_list = self.avcmodule.AVC_getSpecifiedVolumeApps(
            self.thisclass, ctypes.byref(apps_list_size))
        apps_list = []
        for i in range(apps_list_size.value):
            apps_list.append(capps_list[i].decode("utf-8"))
        return apps_list

    ##
    #    Returns a list of programs used by the user.
    ##
    def get_latest_apps(self):
        """Returns a list of programs used by the user."""
        latest_apps_list_size = ctypes.c_int()
        clatest_apps_list = self.avcmodule.AVC_getLatestApps(
            self.thisclass, ctypes.byref(latest_apps_list_size))
        latest_apps_list = []
        for i in range(latest_apps_list_size.value):
            latest_apps_list.append(clatest_apps_list[i].decode("utf-8"))
        return latest_apps_list

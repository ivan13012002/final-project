"""Main menu interface class."""
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_menu_interface.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainMenuWindow(object):
    """User interface for main menu window."""
    def setupUi(self, MainMenuWindow):
        MainMenuWindow.setObjectName("MainMenuWindow")
        MainMenuWindow.resize(600, 400)
        self.centralwidget = QtWidgets.QWidget(MainMenuWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.text_app_name = QtWidgets.QLabel(self.centralwidget)
        self.text_app_name.setGeometry(QtCore.QRect(220, 30, 161, 71))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Emoji")
        self.text_app_name.setFont(font)
        self.text_app_name.setObjectName("text_app_name")
        self.menu_btn_player = QtWidgets.QPushButton(self.centralwidget)
        self.menu_btn_player.setGeometry(QtCore.QRect(220, 120, 161, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.menu_btn_player.setFont(font)
        self.menu_btn_player.setObjectName("menu_btn_player")
        self.menu_btn_vol_controller = QtWidgets.QPushButton(self.centralwidget)
        self.menu_btn_vol_controller.setGeometry(QtCore.QRect(220, 170, 161, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.menu_btn_vol_controller.setFont(font)
        self.menu_btn_vol_controller.setObjectName("menu_btn_vol_controller")
        self.menu_btn_devs = QtWidgets.QPushButton(self.centralwidget)
        self.menu_btn_devs.setGeometry(QtCore.QRect(220, 220, 161, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.menu_btn_devs.setFont(font)
        self.menu_btn_devs.setObjectName("menu_btn_devs")
        self.text_app_version = QtWidgets.QLabel(self.centralwidget)
        self.text_app_version.setGeometry(QtCore.QRect(15, 375, 47, 13))
        self.text_app_version.setObjectName("text_app_version")
        self.menu_btn_exit = QtWidgets.QPushButton(self.centralwidget)
        self.menu_btn_exit.setGeometry(QtCore.QRect(220, 270, 161, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.menu_btn_exit.setFont(font)
        self.menu_btn_exit.setObjectName("menu_btn_exit")
        self.img_logo = QtWidgets.QLabel(self.centralwidget)
        self.img_logo.setGeometry(QtCore.QRect(340, 50, 41, 41))
        self.img_logo.setText("")
        self.img_logo.setObjectName("img_logo")
        self.menu_btn_back = QtWidgets.QPushButton(self.centralwidget)
        self.menu_btn_back.setGeometry(QtCore.QRect(20, 10, 61, 23))
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        font.setPointSize(20)
        self.menu_btn_back.setFont(font)
        self.menu_btn_back.setObjectName("menu_btn_back")
        self.text_devs = QtWidgets.QLabel(self.centralwidget)
        self.text_devs.setGeometry(QtCore.QRect(170, 140, 261, 111))
        self.text_devs.setObjectName("text_devs")
        self.text_header_devs = QtWidgets.QLabel(self.centralwidget)
        self.text_header_devs.setGeometry(QtCore.QRect(170, 30, 271, 71))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Emoji")
        self.text_header_devs.setFont(font)
        self.text_header_devs.setObjectName("text_header_devs")
        self.languages_list = QtWidgets.QComboBox(self.centralwidget)
        self.languages_list.setGeometry(QtCore.QRect(488, 10, 91, 22))
        self.languages_list.setObjectName("languages_list")
        self.languages_list.addItem("")
        self.languages_list.addItem("")
        self.languages_list.addItem("")
        self.languages_list.addItem("")
        MainMenuWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainMenuWindow)
        QtCore.QMetaObject.connectSlotsByName(MainMenuWindow)

    def retranslateUi(self, MainMenuWindow):
        _translate = QtCore.QCoreApplication.translate
        MainMenuWindow.setWindowTitle(_translate("MainMenuWindow", "MainWindow"))
        self.text_app_name.setText(_translate("MainMenuWindow",
                                              "<html><head/><body><p align=\"center\"><span style=\" font-size:20pt; font-weight:600;\">Gridly</span></p></body></html>"))
        self.menu_btn_player.setText(_translate("MainMenuWindow", "Player"))
        self.menu_btn_vol_controller.setText(_translate("MainMenuWindow", "Volume controller"))
        self.menu_btn_devs.setText(_translate("MainMenuWindow", "Developers"))
        self.text_app_version.setText(_translate("MainMenuWindow",
                                                 "<html><head/><body><p><span style=\" font-weight:600;\">v. 1.5</span></p></body></html>"))
        self.menu_btn_exit.setText(_translate("MainMenuWindow", "Exit"))
        self.menu_btn_back.setText(_translate("MainMenuWindow", "🠔"))
        self.text_devs.setText(_translate("MainMenuWindow",
                                          "<html><head/><body><p align=\"center\"><br/></p><p align=\"center\"><span style=\" font-size:12pt;\">QFox Group Ltd.</span></p><p align=\"center\"><span style=\" font-size:12pt;\">Melnikov Mathew, Melnikov Ivan</span></p></body></html>"))
        self.text_header_devs.setText(_translate("MainMenuWindow",
                                                 "<html><head/><body><p align=\"center\"><span style=\" font-size:20pt; font-weight:600;\">Project participants:</span></p></body></html>"))
        self.languages_list.setItemText(0, _translate("MainMenuWindow", "English"))
        self.languages_list.setItemText(1, _translate("MainMenuWindow", "Русский"))
        self.languages_list.setItemText(2, _translate("MainMenuWindow", "Український"))
        self.languages_list.setItemText(3, _translate("MainMenuWindow", "Беларускі"))

"""Creates volume control window and processes actions in it."""

import requests
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QIcon, QPalette, QColor, QFont
from PyQt5.QtWidgets import QMessageBox

from module_conversion import AVCClass
from volume_control_interface import Ui_VolumeControl


def translate(text, input_language, output_language):
    """Перевести текст с одного языка на другой."""
    token = "trnsl.1.1.20200525T174604Z.93ee72b36fca367e.60c2e208c749e9d38d60f2063306e261945096b5"
    url = "https://translate.yandex.net/api/v1.5/tr.json/translate"
    options = {'key': token,
               'lang': input_language + "-" + output_language,
               'text': text}

    try:
        request = requests.get(url, params=options, timeout=1.5).text
    except requests.exceptions.ConnectionError:
        request = None

    if request is None:
        return None
    else:
        return request[36:(len(request) - 3)]


class AutoVolumeControlWindow(QtWidgets.QDialog):
    """Class for processing volume control window."""
    def __init__(self):
        super(AutoVolumeControlWindow, self).__init__()
        self.ui = Ui_VolumeControl()
        self.ui.setupUi(self)
        self.setFixedSize(600, 408)

        self.original_main_title = "Volume control"  # Имя окна плеера
        self.translated_main_title = ""
        self.setWindowTitle(self.original_main_title)

        self.original_adv_title = "Volume control - Advanced settings"
        self.translated_adv_title = ""

        self.setWindowIcon(QIcon("./images/player_icon.jpg"))

        self.volume = 10  # Громкость
        self.range = 5  # Шаг

        # Цвет интерфейса и отдельных элементов
        self.palette = self.palette()
        self.palette.setColor(QPalette.Button, QColor(124, 175, 239))
        self.palette.setColor(QPalette.Highlight, QColor(124, 175, 239))
        self.setPalette(self.palette)

        # Класс контроля звука
        self.avc = AVCClass(self.volume / 100, self.range / 100)

        # Логототип
        volume_logo = QPixmap('./images/sound_logo.png')
        self.ui.image.setPixmap(volume_logo)

        # Кнопки (main settings)
        self.ui.start_stop_button.clicked.connect(self.start_stop_button_clicked)  # Кнопка "Start/Stop"
        # self.start_stop_pos = "Start"
        self.ui.adv_set_button.clicked.connect(self.advanced_settings_button_clicked)  # Кнопка "Расширенные настройки"

        # Ползунок громкости (volume)
        self.ui.volume.valueChanged[int].connect(self.get_volume)
        # Ползунок шага (range)
        self.ui.range.valueChanged[int].connect(self.get_range)

        # Список приложений с настроенным звуком
        self.specified_apps = []

        # Кнопки (advanced settings)
        self.ui.save_specified_button.clicked.connect(self.save_button_clicked)  # Кнопка "Сохранить"
        self.ui.refresh_button.clicked.connect(self.refresh_button_clicked)  # Кнопка "Обновить"
        self.ui.clear_button.clicked.connect(self.clear_button_clicked)  # Кнопка "Очистить всё"
        self.ui.back_button.clicked.connect(self.back_button_clicked)  # Кнопка "Назад" (<-)

        # Поле для ввода названия процесса
        self.ui.app_name.textChanged.connect(self.app_name_entered)
        self.new_app_name = ""

        # Ползунок громкости (volume_specified)
        self.specified_volume = 10
        self.ui.volume_specified.valueChanged[int].connect(self.get_specified_volume)

        # Список приложений с установленной громкостью
        self.ui.list_specified_vol_apps.itemClicked.connect(self.list_specified_app_clicked)

        # Список приложений с запущенными процессами
        self.ui.list_running_apps.itemClicked.connect(self.list_running_app_clicked)

        self.main_settings_lst = [
            self.ui.text_volume,
            self.ui.text_range,
            self.ui.adv_set_button,
            self.ui.volume,
            self.ui.text_volume_max,
            self.ui.text_volume_min,
            self.ui.range,
            self.ui.text_range_max,
            self.ui.text_range_min,
            self.ui.image,
            self.ui.start_stop_button
        ]

        self.advanced_settings_lst = [self.ui.text_running_apps,
                                      self.ui.text_app_name,
                                      self.ui.text_volume_specified,
                                      self.ui.text_specified_vol_apps,
                                      self.ui.save_specified_button,
                                      self.ui.refresh_button,
                                      self.ui.clear_button,
                                      self.ui.text_specified_vol_min,
                                      self.ui.text_specified_vol_max,
                                      self.ui.list_running_apps,
                                      self.ui.list_specified_vol_apps,
                                      self.ui.app_name,
                                      self.ui.volume_specified]

        self.ui.text_no_running.hide()
        self.ui.text_no_specified.hide()

        self.languages = {'English': 'en',
                          'Русский': 'ru',
                          'Español': 'es',
                          'Український': 'uk',
                          'Беларускі': 'be'}

        self.start_stop_button_status = "Start"
        self.start_text = "Start"  # Стандратный текст "Начать"
        self.start_text_translated = "Start"
        self.stop_text = "Stop"  # Стандартный текст "Остановить"
        self.stop_text_translated = "Stop"

        self.original_no_running = "No running apps found"
        self.translated_no_running = "No running apps found"
        self.original_no_specified = "No apps with specified volume found"
        self.translated_no_specified = "No apps with specified volume found"

        self.original_text_delete, self.translated_text_delete = "Delete", "Delete"
        self.original_text_full_delete, self.translated_text_full_delete = "Do you want to delete?", \
                                                                           "Do you want to delete?"

        self.original_text_yes, self.translated_text_yes = "Yes", "Yes"
        self.original_text_cancel, self.translated_text_cancel = "Cancel", "Cancel"

        # Текст на элементах контроля звука (на английском языке)
        self.english_names_main_settings = {
            'text_volume': 'Volume',
            'text_range': 'Range',
            'adv_set_button': 'Advanced settings',
            'volume': '',
            'text_volume_max': '',
            'text_volume_min': '',
            'range': '',
            'text_range_max': '',
            'text_range_min': '',
            'image': '',
            'start_stop_button': ''
        }

        self.english_names_adv_settings = {
            'text_running_apps': 'Running apps',
            'text_app_name': 'App name:',
            'text_volume_specified': 'Volume',
            'text_specified_vol_apps': 'Specified volume apps',
            'save_specified_button': 'Save',
            'refresh_button': 'Refresh',
            'clear_button': 'Clear all',
            'text_specified_vol_min': '',
            'text_specified_vol_max': '',
            'list_running_apps': '',
            'list_specified_vol_apps': '',
            'app_name': '',
            'volume_specified': ''
        }

        # Скрываем расшренные настройки
        self.advanced_settings(False)
        self.main_settings(True)

    def start_stop_button_clicked(self):
        """Обработать кнопку 'Начать/Остановить' (Start/Stop)."""
        if self.avc.is_working():
            if self.start_text != self.start_text_translated:
                self.ui.start_stop_button.setText(self.start_text_translated)
            else:
                self.ui.start_stop_button.setText(self.start_text)

            self.start_stop_button_status = "Stop"
            self.avc.stop_volume_control()
        else:
            if self.stop_text != self.stop_text_translated:
                self.ui.start_stop_button.setText(self.stop_text_translated)
            else:
                self.ui.start_stop_button.setText(self.stop_text)

            self.start_stop_button_status = "Start"
            self.avc.set_default_volume(self.volume / 100)
            self.avc.set_range(self.range / 100)
            self.avc.start_volume_control()

    def get_volume(self, volume):
        """Получить значение ползунка громкости (volume)."""
        self.volume = volume
        # print("Volume:", self.volume)
        if self.avc.is_working():
            self.avc.stop_volume_control()
            self.avc.set_default_volume(self.volume / 100)
            self.avc.start_volume_control()
        else:
            self.avc.set_default_volume(self.volume / 100)

    def get_range(self, _range):
        """Получить значение ползунка шага (range)."""
        self.range = _range
        self.avc.set_range(self.range / 100)

    def main_settings(self, flag):
        """Скрывает/показывает главные настройки AutoVolumeControl в зависимости от флага."""
        if flag is True:
            for element in self.main_settings_lst:
                element.show()

            if self.translated_main_title != '':
                self.setWindowTitle(self.translated_main_title)
            else:
                self.setWindowTitle(self.original_main_title)

        else:
            for element in self.main_settings_lst:
                element.hide()

    def advanced_settings(self, flag):
        """Скрывает/показывает расширенные настройки AutoVolumeControl в зависимости от флага."""
        if flag is True:
            for element in self.advanced_settings_lst:
                element.show()

            if self.translated_adv_title != '':
                self.setWindowTitle(self.translated_adv_title)
            else:
                self.setWindowTitle(self.original_adv_title)

        else:
            for element in self.advanced_settings_lst:
                element.hide()

    def advanced_settings_button_clicked(self):
        """Обработать кнопку 'Расширенные настроки' (Advanced Settings)."""
        self.main_settings(False)
        self.advanced_settings(True)
        self.process_advanced_settings()

    def process_advanced_settings(self):
        """Обработать окно расширенных настроек."""
        self.ui.list_running_apps.clear()
        self.ui.list_specified_vol_apps.clear()

        running_apps = self.avc.get_latest_apps()
        self.specified_apps = self.avc.get_specified_volume_apps()

        if len(running_apps) == 0:
            self.ui.text_no_running.show()
        else:
            for app in running_apps:
                self.ui.list_running_apps.addItem(app)

        if len(self.specified_apps) == 0:
            self.ui.text_no_specified.show()
        else:
            for app in self.specified_apps:
                self.ui.list_specified_vol_apps.addItem(app)

    def refresh_button_clicked(self):
        """Обработать кнопку 'Обновить' (Refresh)."""
        self.ui.list_running_apps.clear()
        self.ui.list_running_apps.addItems(self.avc.get_latest_apps())

    def app_name_entered(self, app_name):
        """Обработать ввод названия процесса."""
        self.new_app_name = app_name

    def get_specified_volume(self, specified_volume):
        """Обработать ввод уровня звука для определённого приложения."""
        self.specified_volume = specified_volume

    def save_button_clicked(self):
        """Обработать кнопку 'Сохранить' (Save)."""
        if self.specified_volume != 0 and self.new_app_name != "":
            self.avc.save_specified_volume(self.new_app_name, self.specified_volume / 100)
            self.process_advanced_settings()

        self.ui.text_no_specified.hide()

    def clear_button_clicked(self):
        """Обработать кноку 'Очистить всё' (Clear all)."""
        self.ui.list_specified_vol_apps.clear()
        self.avc.clear_saved_volumes()

    def list_specified_app_clicked(self, item):
        """Обработать нажатие на элемент списка Specified apps."""
        reply = QMessageBox()
        reply.setIcon(QMessageBox.Critical)
        reply.setWindowIcon(QIcon("./images/player_icon.jpg"))

        if self.translated_text_delete and self.translated_text_full_delete is not None:
            yes_btn = reply.addButton(self.translated_text_yes, QMessageBox.YesRole)
            reply.addButton(self.translated_text_cancel, QMessageBox.NoRole)
            reply.setWindowTitle(self.translated_text_delete)
            reply.setText(self.translated_text_full_delete[:-1] + " '" + item.text() + "'?")

            reply.exec()

            if reply.clickedButton() == yes_btn:
                current_row = self.ui.list_specified_vol_apps.row(self.ui.list_specified_vol_apps.currentItem())

                for i, app in enumerate(self.specified_apps):
                    if app == self.ui.list_specified_vol_apps.currentItem().text():
                        self.avc.delete_specified_volume(i)
                        self.specified_apps = self.avc.get_specified_volume_apps()

                if current_row != -1:
                    self.ui.list_specified_vol_apps.takeItem(current_row)

                if len(self.specified_apps) == 0:
                    self.ui.text_no_specified.show()
        else:
            reply = QMessageBox.critical(self,
                                         self.original_text_delete,
                                         self.original_text_full_delete[:-1] + " '"
                                         + item.text() + "'?",
                                         QMessageBox.Yes | QMessageBox.No)

            if reply == QMessageBox.Yes:
                current_row = self.ui.list_specified_vol_apps.row(self.ui.list_specified_vol_apps.currentItem())

                for i, app in enumerate(self.specified_apps):
                    if app == self.ui.list_specified_vol_apps.currentItem().text():
                        self.avc.delete_specified_volume(i)
                        self.specified_apps = self.avc.get_specified_volume_apps()

                if current_row != -1:
                    self.ui.list_specified_vol_apps.takeItem(current_row)

                if len(self.specified_apps) == 0:
                    self.ui.text_no_specified.show()

    def list_running_app_clicked(self, item):
        """Обработать нажатие на элемент списка Running apps."""
        self.ui.app_name.setText(item.text())

    def back_button_clicked(self):
        """Обработать кнопку 'Назад' (<-)."""
        if self.ui.app_name.isHidden():
            self.ui.list_running_apps.clear()
            self.ui.list_specified_vol_apps.clear()
            self.advanced_settings(False)
            self.close()
        else:
            self.advanced_settings(False)
            self.main_settings(True)
            self.ui.text_no_running.hide()
            self.ui.text_no_specified.hide()

    def translate_interface(self, language):
        """Перевести интерфейс с одного языка на другой."""
        counter = 0
        translated_text = None

        for element, (key, value) in zip(self.main_settings_lst,
                                         self.english_names_main_settings.items()):
            if counter < 3:
                translated_text = translate(value,
                                            'en',
                                            self.languages[language])
                if translated_text is None:
                    break
                else:
                    element.setText(translated_text)
                    counter += 1
            else:
                break

        if translated_text is not None:
            # Окно доп. настроек
            translated_no_running = translate(self.original_no_running,
                                              'en',
                                              self.languages[language])

            translated_no_specified = translate(self.original_no_specified,
                                                'en',
                                                self.languages[language])

            self.ui.text_running_apps.setFont(QFont("Segoe UI", 11, QFont.Bold))
            self.ui.text_specified_vol_apps.setFont(QFont("Segoe UI", 11, QFont.Bold))
            self.ui.text_app_name.setFont(QFont("Segoe UI", 11, QFont.Bold))

            self.translated_adv_title = translate(self.original_adv_title,
                                                  'en',
                                                  self.languages[language])

            self.translated_text_delete = translate(self.original_text_delete,
                                                    'en',
                                                    self.languages[language])

            self.translated_text_full_delete = translate(self.original_text_full_delete,
                                                         'en',
                                                         self.languages[language])

            self.translated_text_yes = translate(self.original_text_yes,
                                                 'en',
                                                 self.languages[language])

            self.translated_text_cancel = translate(self.original_text_cancel,
                                                    'en',
                                                    self.languages[language])

            # Адаптация элементов окна для отдельных языков
            if self.languages[language] == 'ru':
                self.ui.adv_set_button.setGeometry(410, 10, 181, 23)
                self.ui.text_range.setGeometry(433, 190, 181, 21)
                self.ui.text_running_apps.setGeometry(100, 30, 191, 21)
                self.ui.text_specified_vol_apps.setGeometry(300, 30, 241, 21)
                self.ui.text_app_name.setFont(QFont("Segoe UI", 11, QFont.Bold))
                self.ui.text_app_name.setGeometry(90, 228, 171, 21)

                self.ui.text_no_running.setText(translated_no_running.split()[0] + " " +
                                                translated_no_running.split()[1] + "\n" +
                                                translated_no_running.split()[2] + "\n")

                self.ui.text_no_running.setAlignment(Qt.AlignCenter)

                self.ui.text_no_specified.setText(translated_no_specified.split()[0] + " " +
                                                  translated_no_specified.split()[1] + "\n" +
                                                  translated_no_specified.split()[2] + " " +
                                                  translated_no_specified.split()[3] + "\n" +
                                                  translated_no_specified.split()[4])

                self.ui.text_no_specified.setAlignment(Qt.AlignCenter)
            elif self.languages[language] == 'uk':
                self.ui.adv_set_button.setGeometry(460, 10, 131, 23)
                self.ui.text_range.setGeometry(433, 190, 181, 21)
                self.ui.text_running_apps.setGeometry(120, 30, 161, 21)
                self.ui.text_specified_vol_apps.setGeometry(320, 30, 181, 21)
                self.ui.text_app_name.setFont(QFont("Segoe UI", 11, QFont.Bold))
                self.ui.text_app_name.setGeometry(160, 230, 121, 21)
                self.ui.text_volume.setGeometry(445, 100, 181, 21)
            elif self.languages[language] == 'be':
                self.ui.adv_set_button.setGeometry(450, 10, 141, 23)
                self.ui.text_range.setGeometry(433, 190, 181, 21)
                self.ui.text_running_apps.setGeometry(80, 30, 211, 21)
                self.ui.text_specified_vol_apps.setGeometry(300, 30, 231, 21)
                self.ui.text_app_name.setFont(QFont("Segoe UI", 11, QFont.Bold))
                self.ui.text_app_name.setGeometry(115, 229, 151, 21)
                self.ui.text_volume.setGeometry(445, 100, 181, 21)

                self.ui.text_no_running.setText(translated_no_running.split()[0] + " " +
                                                translated_no_running.split()[1] + "\n" +
                                                translated_no_running.split()[2] + "\n")

                self.ui.text_no_running.setAlignment(Qt.AlignCenter)

                self.ui.text_no_specified.setText(translated_no_specified.split()[0] + " " +
                                                  translated_no_specified.split()[1] + "\n" +
                                                  translated_no_specified.split()[2] + " " +
                                                  translated_no_specified.split()[3] + "\n" +
                                                  translated_no_specified.split()[4])

                self.ui.text_no_specified.setAlignment(Qt.AlignCenter)
            elif self.languages[language] == 'en':
                self.ui.adv_set_button.setGeometry(470, 10, 121, 23)
                self.ui.text_range.setGeometry(445, 190, 181, 21)
                self.ui.text_running_apps.setGeometry(150, 30, 101, 21)
                self.ui.text_specified_vol_apps.setGeometry(330, 30, 161, 21)
                self.ui.text_app_name.setFont(QFont("Segoe UI", 11, QFont.Bold))
                self.ui.text_app_name.setGeometry(160, 230, 81, 21)
                self.ui.text_volume.setGeometry(440, 100, 181, 21)

                self.ui.text_no_running.setText(self.original_no_running.split()[0] + " " +
                                                self.original_no_running.split()[1] + "\n" +
                                                self.original_no_running.split()[2] + "\n")

                self.ui.text_no_running.setAlignment(Qt.AlignCenter)

                self.ui.text_no_specified.setText(self.original_no_running.split()[0] + " " +
                                                  self.original_no_running.split()[1] + "\n" +
                                                  self.original_no_running.split()[2] + " " +
                                                  translated_no_specified.split()[3])

            self.start_text_translated = translate(self.start_text,
                                                   'en',
                                                   self.languages[language])
            self.stop_text_translated = translate(self.stop_text,
                                                  'en',
                                                  self.languages[language])

            self.translated_main_title = translate(self.original_main_title,
                                                   'en',
                                                   self.languages[language])

            if self.start_stop_button_status == "Stop":
                self.ui.start_stop_button.setText(self.stop_text_translated)
            else:
                self.ui.start_stop_button.setText(self.start_text_translated)

            self.setWindowTitle(self.translated_main_title)

        # Перевод окна доп. настроек
        counter = 0

        for element, (key, value) in zip(self.advanced_settings_lst,
                                         self.english_names_adv_settings.items()):
            if counter > 6:
                break
            elif value != "":
                translated_text = translate(value,
                                            'en',
                                            self.languages[language])
                if translated_text is None:
                    break
                else:
                    element.setText(translated_text)
                    counter += 1

var indexSectionsWithContent =
{
  0: "_abcdfgilmnprstuv",
  1: "amptu",
  2: "f",
  3: "ampv",
  4: "_abcdfgilmnprst",
  5: "a",
  6: "t",
  7: "g"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "groups",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Modules",
  7: "Pages"
};


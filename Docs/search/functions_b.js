var searchData=
[
  ['pause_251',['pause',['../group___a_v_c_p__module.html#ga787a0ad3fb56c14fd905a6f60bb6bf69',1,'AutoVolumeControlPlayer::pause()'],['../classfinal-project_1_1module__conversion__p_1_1_a_v_c_p_class.html#ad8288a8ec44b338aaf4220709f373b98',1,'final-project.module_conversion_p.AVCPClass.pause()']]],
  ['play_252',['play',['../group___a_v_c_p__module.html#ga13e2215722c900d0637f612bed3bd71f',1,'AutoVolumeControlPlayer::play()'],['../classfinal-project_1_1module__conversion__p_1_1_a_v_c_p_class.html#a9383588dc5a1a7745de3e5e7e4081b97',1,'final-project.module_conversion_p.AVCPClass.play()']]],
  ['play_5fnext_253',['play_next',['../classfinal-project_1_1module__conversion__p_1_1_a_v_c_p_class.html#a90c56660615a7715a7da070d4026dbf9',1,'final-project::module_conversion_p::AVCPClass']]],
  ['play_5fprevious_254',['play_previous',['../classfinal-project_1_1module__conversion__p_1_1_a_v_c_p_class.html#a7168e939418fa7249ac9bc30b84c17f4',1,'final-project::module_conversion_p::AVCPClass']]],
  ['playlist_5fwindow_255',['playlist_window',['../classfinal-project_1_1player__window_1_1_player_window.html#ad8d5b3eb83fe49d814d0a4937f8c2358',1,'final-project::player_window::PlayerWindow']]],
  ['playnext_256',['playNext',['../group___a_v_c_p__module.html#ga0e8e5a8684f7743539d321e80170f6b0',1,'AutoVolumeControlPlayer']]],
  ['playprevious_257',['playPrevious',['../group___a_v_c_p__module.html#ga82064dbfee733c5f44edc7f3f90416a4',1,'AutoVolumeControlPlayer']]],
  ['process_5fadvanced_5fsettings_258',['process_advanced_settings',['../classfinal-project_1_1volume__control__window_1_1_auto_volume_control_window.html#acfb9703e126427a6faf9cf292da583eb',1,'final-project::volume_control_window::AutoVolumeControlWindow']]]
];

var searchData=
[
  ['the_20avc_5fmodule_134',['The AVC_module',['../group___a_v_c__module.html',1,'']]],
  ['the_20avcp_5fmodule_135',['The AVCP_module',['../group___a_v_c_p__module.html',1,'']]],
  ['testavcclass_136',['TestAVCClass',['../classfinal-project_1_1test__avc_1_1_test_a_v_c_class.html',1,'final-project::test_avc']]],
  ['testavcpclass_137',['TestAVCPClass',['../classfinal-project_1_1test__avcp_1_1_test_a_v_c_p_class.html',1,'final-project::test_avcp']]],
  ['tracktimethread_138',['TrackTimeThread',['../classfinal-project_1_1player__window_1_1_track_time_thread.html',1,'final-project::player_window']]],
  ['translate_139',['translate',['../main__menu__window_8py.html#a985df34dfcb918f14dc4248d46e01f66',1,'final-project.main_menu_window.translate()'],['../player__window_8py.html#a46224cfe8fcc6d1fcceea95dd23299a2',1,'final-project.player_window.translate()'],['../volume__control__window_8py.html#a19852b1f9183ff514767485ece7df20b',1,'final-project.volume_control_window.translate()']]],
  ['translate_5finterface_140',['translate_interface',['../classfinal-project_1_1main__menu__window_1_1_main_menu_window.html#a2d5ca7626c74a23dceaec6eb5bc13a42',1,'final-project.main_menu_window.MainMenuWindow.translate_interface()'],['../classfinal-project_1_1player__window_1_1_player_window.html#a7e3cdfba9b8a372fabfa5d9009e5bd6f',1,'final-project.player_window.PlayerWindow.translate_interface()'],['../classfinal-project_1_1volume__control__window_1_1_auto_volume_control_window.html#a633526445a0bd8b22f918c7d417b8e0e',1,'final-project.volume_control_window.AutoVolumeControlWindow.translate_interface()']]]
];

"""Creates player window and processes actions in it."""

import time

import requests
from PyQt5 import QtWidgets
from PyQt5.QtCore import QThread, Qt
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtGui import QPalette, QColor, QFont
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QInputDialog

from module_conversion_p import AVCPClass
from player_interface import Ui_Player


def translate(text, input_language, output_language):
    """Перевести текст с одного языка на другой."""
    token = "trnsl.1.1.20200525T174604Z.93ee72b36fca367e.60c2e208c749e9d38d60f2063306e261945096b5"
    url = "https://translate.yandex.net/api/v1.5/tr.json/translate"
    options = {'key': token,
               'lang': input_language + "-" + output_language,
               'text': text}

    try:
        request = requests.get(url, params=options, timeout=1.5).text
    except requests.exceptions.ConnectionError:
        request = None

    if request is None:
        return None
    else:
        return request[36:(len(request) - 3)]


class TrackTimeThread(QThread):
    """Class for putting some operations into a separate thread."""
    def __init__(self, player_window, parent=None):
        super(TrackTimeThread, self).__init__(parent)
        self.player_window = player_window

    def run(self):
        while True:
            minutes_passed, seconds_passed = \
                '{:02}'.format(self.player_window.avcp.get_time_now_dict()['Minutes']), \
                '{:02}'.format(self.player_window.avcp.get_time_now_dict()['Seconds'])
            self.player_window.ui.text_time_passed.setText(minutes_passed + ':' + seconds_passed)
            # print(minutes_passed + ":" + seconds_passed)

            minutes_remained, seconds_remained = '{:02}'.format(
                self.player_window.avcp.get_remaining_time_dict()['Minutes']), \
                                                 '{:02}'.format(
                                                     self.player_window.avcp.get_remaining_time_dict()['Seconds'])
            self.player_window.ui.text_time_remained.setText(minutes_remained + ':' + seconds_remained)

            if not self.player_window.avcp.get_track_info()['is_paused']:
                if self.player_window.ui.track_bar.value() >= int(self.player_window.avcp.get_time()):
                    self.player_window.ui.text_time_remained.setText('00' + ':' + '00')
                    self.player_window.ui.text_time_passed.setText('00' + ':' + '00')
                    self.player_window.btn_next_clicked()
                else:
                    self.player_window.ui.track_bar.setValue(self.player_window.value + 1)
                    self.player_window.value += 1

            self.sleep(1)


class PlayerWindow(QtWidgets.QDialog):
    """Class for main menu window processing."""
    def __init__(self):
        super(PlayerWindow, self).__init__()
        self.ui = Ui_Player()
        self.ui.setupUi(self)
        self.setFixedSize(600, 408)  # Фиксированный размер окна

        self.original_player_title = "Player"  # Имя окна плеера
        self.translated_player_title = ""
        self.setWindowTitle(self.original_player_title)

        self.original_playlists_title = "Player - Playlists"
        self.translated_playlists_title = ""

        self.setWindowIcon(QIcon("./images/player_icon.jpg"))

        self.thread = TrackTimeThread(player_window=self)

        self.value = 0

        # Класс контроля плеера
        self.avcp = AVCPClass()
        self.flag_chosen = False  # Выбран ли трек для проигрывания

        # Цвет интерфейса и отдельных элементов
        self.palette = self.palette()
        self.palette.setColor(QPalette.Button, QColor(124, 175, 239))
        self.palette.setColor(QPalette.Highlight, QColor(124, 175, 239))
        self.setPalette(self.palette)

        track_logo_image = QPixmap('./images/music.png')
        self.ui.track_image.setPixmap(track_logo_image)

        # Кнопки
        self.ui.play_pause_button.clicked.connect(self.btn_play_clicked)  # Кнопка "Играть/Остановить"
        self.btn_play_status = "Play"
        self.ui.next_button.clicked.connect(self.btn_next_clicked)  # Кнопка "Следуюший трек"
        self.ui.prev_button.clicked.connect(self.btn_prev_clicked)  # Кнопка "Предыдущий трек"
        self.ui.open_track_button.clicked.connect(self.btn_open_track_clicked)  # Кнопка "Открыть"
        self.ui.playlist_button.clicked.connect(self.btn_playlist_clicked)  # Кнопка "Плейлисты"

        # Слайдер (track_bar)
        self.ui.track_bar.sliderReleased.connect(self.slider_moved)  # Слайдер воспроизведения трека

        # Элементы главного окна контроля звука
        self.player_window_elements_lst = [self.ui.open_track_button,
                                           self.ui.playlist_button,
                                           self.ui.text_track_name,
                                           self.ui.prev_button,
                                           self.ui.next_button,
                                           self.ui.text_sample_rate,
                                           self.ui.text_track_volume,
                                           self.ui.text_channel_count,
                                           self.ui.sample_rate,
                                           self.ui.track_volume,
                                           self.ui.channel_count,
                                           self.ui.text_time_passed,
                                           self.ui.text_time_remained,
                                           self.ui.track_bar,
                                           self.ui.play_pause_button,
                                           self.ui.track_image]

        # Кнопка возврата
        self.ui.back_button.clicked.connect(self.btn_back_clicked)

        self.languages = {'English': 'en',
                          'Русский': 'ru',
                          'Español': 'es',
                          'Український': 'uk',
                          'Беларускі': 'be'}

        # Текст на элементах плеера (на английском языке)
        self.english_names_player = {'open_track_button': 'Open',
                                     'playlist_button': 'Playlists',
                                     'text_track_name': 'Playlist: Track name',
                                     'prev_button': 'Previous',
                                     'next_button': 'Next',
                                     'text_sample_rate': 'Sample rate',
                                     'text_track_volume': 'Track volume',
                                     'text_channel_count': "Channel's number",
                                     'sample_rate': '',
                                     'track_volume': '',
                                     'channel count': '',
                                     'text_time_passed': '',
                                     'text_time_remained': '',
                                     'track_bar': '',
                                     'play_pause_button': '',
                                     'track_image': ''}

        self.english_names_playlists = {
            'create_playlist_button': 'Create new playlist',
            'text_playlists': 'Playlists',
            'text_tracks': 'Tracks',
            'text_playlist_name': "Playlist's name:",
            'new_playlist_name': '',
            'list_tracks': '',
            'list_playlists': '',
        }

        self.original_text_delete, self.translated_text_delete = "Delete", "Delete"
        self.original_text_full_delete, self.translated_text_full_delete = "Do you want to delete?", \
                                                                           "Do you want to delete?"

        self.original_text_yes, self.translated_text_yes = "Yes", "Yes"
        self.original_text_cancel, self.translated_text_cancel = "Cancel", "Cancel"

        self.original_text_play_playlist_btn, self.translated_text_play_playlist_btn = 'Play', \
                                                                                       'Play'
        self.original_text_add_track_btn, self.translated_text_add_track_btn = 'Add track', \
                                                                               'Add track'
        self.original_text_rename_playlist_btn, self.translated_text_rename_playlist_btn = 'Rename', \
                                                                                           'Rename'
        self.original_text_delete_playlist_btn, self.translated_text_delete_playlist_btn = 'Delete', \
                                                                                           'Delete'
        self.original_text_info_playlists, self.translated_text_info_playlists = \
            'What do you want to do with playlist?', 'What do you want to do with playlist?'

        self.original_text_choose_track, self.translated_text_choose_track = \
            'Choose track', 'Choose track'

        self.original_text_enter_playlist_name, self.translated_text_enter_playlist_name = \
            'Enter new name of playlist:', 'Enter new name of playlist:'

        self.original_text_choose_directory, self.translated_text_choose_directory = \
            'Choose directory', 'Choose directory'

        self.original_text_error, self.translated_text_error = 'Error', 'Error'

        self.original_text_error_exists, self.translated_text_error_exists = \
            'Playlist with this name already exists!', \
            'Playlist with this name already exists!'

        self.original_text_error_name, self.translated_text_error_name = \
            'Please, enter the name of the playlist!', 'Please, enter the name of the playlist!'

        self.play_text = "Play"  # Стандратный текст "Начать"
        self.play_text_translated = "Play"
        self.pause_text = "Stop"  # Стандартный текст "Остановить"
        self.pause_text_translated = "Stop"

        # --- Плейлисты (отдельное окно) --- #
        self.new_playlist_name = ""  # Имя нового плейлиста
        self.current_playlist = None  # Выбранный плейлист

        # Элементы отдельного окна "Плейлисты" родительского окна плеера
        self.playlist_window_elements_lst = [self.ui.create_playlist_button,
                                             self.ui.text_playlists,
                                             self.ui.text_tracks,
                                             self.ui.text_playlist_name,
                                             self.ui.new_playlist_name,
                                             self.ui.list_tracks,
                                             self.ui.list_playlists]

        self.ui.list_playlists.itemClicked.connect(self.list_playlist_clicked)  # Список плейлистов
        self.ui.list_tracks.itemClicked.connect(self.list_track_clicked)  # Список треков плейлиста

        self.ui.create_playlist_button.clicked.connect(self.btn_create_playlist_clicked)  # Кнопка "Новый плейлист"

        self.ui.new_playlist_name.textChanged.connect(self.new_playlist_name_entered)

        self.main_player_window(True)
        self.playlist_window(False)

        self.ui.sample_rate.hide()
        self.ui.channel_count.hide()
        self.ui.track_volume.hide()

    def btn_play_clicked(self):
        """Обработать кнопку 'Играть/Остановить'."""
        if not self.avcp.get_queue():
            return

        # Смена надписи на кнопке "Играть/Остановить"
        if self.btn_play_status == "Play" and self.flag_chosen is True:
            self.btn_play_status = "Stop"

            if self.pause_text != self.pause_text_translated:
                self.ui.play_pause_button.setText(self.pause_text_translated)
            else:
                self.ui.play_pause_button.setText(self.pause_text)

            # self.ui.play_pause_button.setText(self.btn_play_status)

            self.avcp.play()

            while self.avcp.get_track_info()['is_processing']:
                time.sleep(0.2)

            if self.avcp.get_track_info()['playlist'] == "None":
                self.ui.text_track_name.setText(self.avcp.get_track_info()['title'][:-4])
            else:
                index = self.avcp.get_track_info()['title'].find('/', self.avcp.get_track_info()['title'].find('/') + 1)
                title = self.avcp.get_track_info()['title'][index + 1:-4]
                self.ui.text_track_name.setText(self.avcp.get_track_info()['playlist'] + ": " + title)

            self.ui.text_track_name.setFont(QFont("MS Shell Dlg 2", 11, QFont.Bold))
            self.ui.text_track_name.setAlignment(Qt.AlignCenter)
            self.palette.setColor(QPalette.Button, QColor(124, 175, 239))
            self.palette.setColor(QPalette.Highlight, QColor(124, 175, 239))
            self.setPalette(self.palette)

            #
            self.ui.sample_rate.setText(str(self.avcp.get_track_info()['sample_rate']))
            self.ui.track_volume.setText(str(int(self.avcp.get_track_info()['volume'] * 100)))
            self.ui.channel_count.setText(str(self.avcp.get_track_info()['channel_count']))

            self.ui.sample_rate.setFont(QFont("MS Shell Dlg 2", 8, QFont.Bold))
            self.ui.track_volume.setFont(QFont("MS Shell Dlg 2", 8, QFont.Bold))
            self.ui.channel_count.setFont(QFont("MS Shell Dlg 2", 8, QFont.Bold))

            if self.ui.list_tracks.isHidden() and self.flag_chosen is True:
                self.ui.sample_rate.show()
                self.ui.channel_count.show()
                self.ui.track_volume.show()
            else:
                self.ui.sample_rate.hide()
                self.ui.channel_count.hide()
                self.ui.track_volume.hide()
            #

            self.ui.track_bar.setMaximum(self.avcp.get_time())

            if not self.thread.isRunning():
                # print("Process is running!", end="")
                self.thread.start()  # Запускаем поток контроля времени проигрывания трека и трек-бара
                # print(" ", self.thread.isRunning(), sep="")
        elif self.btn_play_status == "Play" and self.flag_chosen is False:
            pass
        else:
            self.btn_play_status = "Play"

            if self.play_text != self.play_text_translated:
                self.ui.play_pause_button.setText(self.play_text_translated)
            else:
                self.ui.play_pause_button.setText(self.play_text)

            # self.ui.play_pause_button.setText(self.btn_play_status)

            if self.thread.isRunning():
                self.thread.terminate()
                # print("Process is stopped!")

            self.avcp.pause()

    def btn_next_clicked(self):
        """Обработать кнопку 'Следующий трек' (Next)."""
        if not self.avcp.get_queue():
            return

        self.thread.terminate()

        self.avcp.play_next()
        self.value = 0
        self.ui.track_bar.setValue(0)

        self.btn_play_status = "Stop"

        if self.pause_text_translated != '' or None:
            self.ui.play_pause_button.setText(self.pause_text_translated)
        else:
            self.ui.play_pause_button.setText(self.pause_translated)

        self.avcp.play()

        while self.avcp.get_track_info()['is_processing']:
            time.sleep(0.2)

        if self.avcp.get_track_info()['playlist'] == "None":
            self.ui.text_track_name.setText(self.avcp.get_track_info()['title'][:-4])
        else:
            index = self.avcp.get_track_info()['title'].find('/', self.avcp.get_track_info()['title'].find('/') + 1)
            title = self.avcp.get_track_info()['title'][index + 1:-4]
            self.ui.text_track_name.setText(self.avcp.get_track_info()['playlist'] + ": " + title)

        # self.ui.text_track_name.setText(self.avcp.get_track_info()['title'][:-4])
        self.ui.track_bar.setMaximum(self.avcp.get_time())

        #
        self.ui.sample_rate.setText(str(self.avcp.get_track_info()['sample_rate']))
        self.ui.track_volume.setText(str(int(self.avcp.get_track_info()['volume'] * 100)))
        self.ui.channel_count.setText(str(self.avcp.get_track_info()['channel_count']))
        #

        if self.thread.isFinished():
            self.thread.start()
            # print("Process is running!")

    def btn_prev_clicked(self):
        """Обработать кнопку 'Предыдущий трек' (Previous)."""
        if not self.avcp.get_queue():
            return

        if self.thread.isRunning():
            self.thread.terminate()
            # print("Process is stopped!")

        self.avcp.play_previous()
        self.value = 0
        self.ui.track_bar.setValue(0)

        while self.avcp.get_track_info()['is_processing']:
            time.sleep(0.2)

        if self.avcp.get_track_info()['playlist'] == "None":
            self.ui.text_track_name.setText(self.avcp.get_track_info()['title'][:-4])
        else:
            index = self.avcp.get_track_info()['title'].find('/', self.avcp.get_track_info()['title'].find('/') + 1)
            title = self.avcp.get_track_info()['title'][index + 1:-4]
            self.ui.text_track_name.setText(self.avcp.get_track_info()['playlist'] + ": " + title)
        # self.ui.text_track_name.setText(self.avcp.get_track_info()['title'][:-4])

        #
        self.ui.sample_rate.setText(str(self.avcp.get_track_info()['sample_rate']))
        self.ui.track_volume.setText(str(int(self.avcp.get_track_info()['volume'] * 100)))
        self.ui.channel_count.setText(str(self.avcp.get_track_info()['channel_count']))
        #

        self.ui.track_bar.setMaximum(self.avcp.get_time())

        if not self.thread.isRunning():
            self.thread.start()

    def btn_open_track_clicked(self):
        """Обработать кнопку 'Открыть папку' (Open)."""
        if self.translated_text_choose_directory != "" or None:
            directory = QFileDialog.getExistingDirectory(self, self.translated_text_choose_directory,
                                                         "/home")
        else:
            directory = QFileDialog.getExistingDirectory(self, self.original_text_choose_directory,
                                                         "/home")

        self.avcp.set_path(directory)

        self.btn_play_status = "Play"

        if self.play_text_translated != '' or None:
            self.ui.play_pause_button.setText(self.play_text_translated)
        else:
            self.ui.play_pause_button.setText(self.play_text)

        self.flag_chosen = True

        if self.thread.isRunning():
            self.thread.terminate()
            # ("Process is stopped!")

        self.avcp.stop()

    def slider_moved(self):
        """Обработать изменения положения слайдера track_bar."""
        new_value = self.ui.track_bar.value()
        self.avcp.set_time_now(new_value)
        self.value = new_value

        if self.thread.isRunning():
            self.thread.terminate()
            # print("Process is stopped!")
            time.sleep(1)
            self.thread.start()
            # print("Process is running!")
        else:
            self.thread.start()
            # print("Process is running!")

    def main_player_window(self, flag):
        """Скрывает/показывает элементы главного окна плеера."""
        if flag is True:
            for element in self.player_window_elements_lst:
                element.show()

            if self.translated_playlists_title != "":
                self.setWindowTitle(self.translated_player_title)
            else:
                self.setWindowTitle(self.original_player_title)
        else:
            for element in self.player_window_elements_lst:
                element.hide()

    def playlist_window(self, flag):
        """Скрывает/показывает элементы окна плейлистов."""
        if flag is True:
            for element in self.playlist_window_elements_lst:
                element.show()
            if self.translated_player_title != "":
                self.setWindowTitle(self.translated_playlists_title)
            else:
                self.setWindowTitle(self.original_playlists_title)
        else:
            for element in self.playlist_window_elements_lst:
                element.hide()

    def btn_playlist_clicked(self):
        """Обработать нажатие кнопки 'Плейлисты' (Playlists)."""
        self.main_player_window(False)
        self.playlist_window(True)
        self.ui.list_playlists.addItems(self.avcp.get_playlists())

    def new_playlist_name_entered(self, playlist_name):
        """Обработать введённое имя нового плейлиста."""
        self.new_playlist_name = playlist_name

    def btn_create_playlist_clicked(self):
        """Обработать нажатие кнопки 'Новый плейлист' (New playlist)."""
        if self.new_playlist_name != "":
            if self.avcp.find_playlist(str(self.new_playlist_name)) is False:
                self.avcp.create_playlist(str(self.new_playlist_name))
                self.ui.list_playlists.addItem(str(self.new_playlist_name))
            else:
                if self.translated_text_error != '' or None:
                    QMessageBox.warning(self, self.translated_text_error,
                                        self.translated_text_error_exists)
                else:
                    QMessageBox.warning(self, self.original_text_error,
                                        self.original_text_error_exists)
        else:
            if self.translated_text_error != '' or None:
                QMessageBox.warning(self, self.translated_text_error,
                                    self.translated_text_error_name)
            else:
                QMessageBox.warning(self, self.original_text_error,
                                    self.original_text_error_name)

    def list_playlist_clicked(self, item):
        """Обработать нажатие на элемент списка плейлистов."""
        self.current_playlist = item.text()

        self.ui.list_tracks.clear()
        self.ui.list_tracks.addItems(self.avcp.get_tracks(item.text()))

        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowIcon(QIcon("./images/player_icon.jpg"))

        if self.translated_player_title != "":
            msg.setWindowTitle(self.translated_playlists_title)
        else:
            msg.setWindowTitle(self.original_playlists_title)

        if self.translated_text_info_playlists != self.original_text_info_playlists:
            msg.setText(self.translated_text_info_playlists[:-1] + " '" + item.text() + "'?")
        else:
            msg.setText(self.original_text_info_playlists[:-1] + " '" + item.text() + "'?")

        play_playlist_btn = msg.addButton(self.translated_text_play_playlist_btn,
                                          QMessageBox.ActionRole)
        add_track_btn = msg.addButton(self.translated_text_add_track_btn,
                                      QMessageBox.ActionRole)
        rename_playlist_btn = msg.addButton(self.translated_text_rename_playlist_btn,
                                            QMessageBox.ActionRole)
        delete_playlist_btn = msg.addButton(self.translated_text_delete_playlist_btn,
                                            QMessageBox.HelpRole)
        cancel_button = msg.addButton('Cancel', QMessageBox.RejectRole)
        cancel_button.hide()

        msg.exec()

        if msg.clickedButton() == play_playlist_btn:
            if self.thread.isRunning():
                self.thread.terminate()

            if self.avcp.get_track_info()['is_playing'] is True:
                self.btn_play_status = "Play"
                self.avcp.stop()
                # time.sleep(1)

            self.avcp.start_playlist(item.text())
            self.flag_chosen = True
            #
            if self.btn_play_status == "Play":
                self.btn_play_status = "Stop"

                if self.pause_text_translated != '' or None:
                    self.ui.play_pause_button.setText(self.pause_text_translated)
                else:
                    self.ui.play_pause_button.setText(self.pause_text)

                while self.avcp.get_track_info()['is_processing']:
                    time.sleep(0.2)

                index = self.avcp.get_track_info()['title'].find('/', self.avcp.get_track_info()['title'].find('/') + 1)
                title = self.avcp.get_track_info()['title'][index + 1:-4]
                self.ui.text_track_name.setText(item.text() + ": " + title)
                self.ui.text_track_name.setFont(QFont("MS Shell Dlg 2", 11, QFont.Bold))
                self.ui.text_track_name.setAlignment(Qt.AlignCenter)
                self.palette.setColor(QPalette.Button, QColor(124, 175, 239))
                self.palette.setColor(QPalette.Highlight, QColor(124, 175, 239))
                self.setPalette(self.palette)

                #
                self.ui.sample_rate.setText(str(self.avcp.get_track_info()['sample_rate']))
                self.ui.track_volume.setText(str(int(self.avcp.get_track_info()['volume'] * 100)))
                self.ui.channel_count.setText(str(self.avcp.get_track_info()['channel_count']))

                self.ui.sample_rate.setFont(QFont("MS Shell Dlg 2", 8, QFont.Bold))
                self.ui.track_volume.setFont(QFont("MS Shell Dlg 2", 8, QFont.Bold))
                self.ui.channel_count.setFont(QFont("MS Shell Dlg 2", 8, QFont.Bold))

                if self.ui.list_tracks.isHidden():
                    self.ui.sample_rate.show()
                    self.ui.channel_count.show()
                    self.ui.track_volume.show()
                else:
                    self.ui.sample_rate.hide()
                    self.ui.channel_count.hide()
                    self.ui.track_volume.hide()
                #

                self.ui.track_bar.setMaximum(self.avcp.get_time())

                if not self.thread.isRunning():
                    # print("Process is running!", end="")
                    self.thread.start()  # Запускаем поток контроля времени проигрывания трека и трек-бара
                    # print(" ", self.thread.isRunning(), sep="")
            else:
                self.btn_play_status = "Play"

                if self.play_text_translated != '' or None:
                    self.ui.play_pause_button.setText(self.play_text_translated)
                else:
                    self.ui.play_pause_button.setText(self.play_text)
                # self.ui.play_pause_button.setText(self.btn_play_status)

                if self.thread.isRunning():
                    self.thread.terminate()
                    # print("Process is stopped!")

                self.avcp.pause()
            #
        elif msg.clickedButton() == add_track_btn:
            track_path = QFileDialog.getOpenFileName(self, self.translated_text_choose_track)
            # print(track_path[0])
            self.avcp.add_track_to_playlist(track_path[0], item.text())
            self.ui.list_tracks.clear()
            self.ui.list_tracks.addItems(self.avcp.get_tracks(item.text()))
        elif msg.clickedButton() == rename_playlist_btn:
            if self.translated_playlists_title != "":
                playlist_new_name, ok = QInputDialog.getText(self, self.translated_playlists_title,
                                                             self.translated_text_enter_playlist_name)
            else:
                playlist_new_name, ok = QInputDialog.getText(self, self.original_playlists_title,
                                                             self.original_text_enter_playlist_name)

            if ok:
                self.avcp.rename_playlist(str(playlist_new_name), item.text())
                self.ui.list_playlists.clear()
                self.ui.list_playlists.addItems(self.avcp.get_playlists())
        elif msg.clickedButton() == delete_playlist_btn:
            current_row = self.ui.list_playlists.row(self.ui.list_playlists.currentItem())

            if current_row != -1:
                self.ui.list_playlists.takeItem(current_row)

            self.avcp.delete_playlist(item.text())

    def list_track_clicked(self, item):
        """Обработать нажатие на элемент списка треков."""
        reply = QMessageBox()
        reply.setIcon(QMessageBox.Critical)
        reply.setWindowIcon(QIcon("./images/player_icon.jpg"))

        if self.translated_text_delete and self.translated_text_full_delete is not None:
            yes_btn = reply.addButton(self.translated_text_yes, QMessageBox.YesRole)
            reply.addButton(self.translated_text_cancel, QMessageBox.NoRole)
            reply.setWindowTitle(self.translated_text_delete)
            reply.setText(self.translated_text_full_delete[:-1] + "'" + item.text() + "'?")

            reply.exec()

            if reply.clickedButton() == yes_btn:
                current_row = self.ui.list_tracks.row(self.ui.list_tracks.currentItem())

                if self.avcp.find_track(item.text(), self.current_playlist) is True:
                    self.avcp.delete_track_from_playlist(item.text(), self.current_playlist)

                if current_row != -1:
                    self.ui.list_tracks.takeItem(current_row)
        else:
            reply = QMessageBox.critical(self,
                                         self.original_text_delete,
                                         self.original_text_full_delete[:-1] + " '"
                                         + item.text() + "'?",
                                         QMessageBox.Yes | QMessageBox.No)

            if reply == QMessageBox.Yes:
                current_row = self.ui.list_tracks.row(self.ui.list_tracks.currentItem())

                if self.avcp.find_track(item.text(), self.current_playlist) is True:
                    self.avcp.delete_track_from_playlist(item.text(), self.current_playlist)

                if current_row != -1:
                    self.ui.list_tracks.takeItem(current_row)

    def btn_back_clicked(self):
        """Обработать нажатие на кнопку 'Назад' (Back)."""
        if self.ui.list_playlists.isHidden():
            self.playlist_window(False)
            self.ui.list_playlists.clear()
            self.ui.list_tracks.clear()
            self.close()
        else:
            self.ui.list_playlists.clear()
            self.ui.list_tracks.clear()
            self.playlist_window(False)
            self.main_player_window(True)

            if self.flag_chosen is False:
                self.ui.sample_rate.hide()
                self.ui.channel_count.hide()
                self.ui.track_volume.hide()

    def translate_interface(self, language):
        """Перевести интерфейс с одного языка на другой."""
        counter = 0
        translated_text = None

        for element, (key, value) in zip(self.player_window_elements_lst, self.english_names_player.items()):
            if counter > 8:
                break
            else:
                translated_text = translate(value,
                                            'en',
                                            self.languages[language])
                if translated_text is None:
                    break
                else:
                    element.setText(translated_text)
                    counter += 1

        if translated_text is not None:
            self.ui.text_track_name.setFont(QFont("MS Shell Dlg 2", 11, QFont.Bold))
            self.ui.text_track_name.setAlignment(Qt.AlignCenter)

            self.translated_text_delete = translate(self.original_text_delete,
                                                    'en',
                                                    self.languages[language])

            self.translated_text_full_delete = translate(self.original_text_full_delete,
                                                         'en',
                                                         self.languages[language])

            self.translated_text_yes = translate(self.original_text_yes,
                                                 'en',
                                                 self.languages[language])

            self.translated_text_cancel = translate(self.original_text_cancel,
                                                    'en',
                                                    self.languages[language])

            self.translated_text_play_playlist_btn = translate(self.original_text_play_playlist_btn,
                                                               'en',
                                                               self.languages[language])

            self.translated_text_add_track_btn = translate(self.original_text_add_track_btn,
                                                           'en',
                                                           self.languages[language])

            self.translated_text_rename_playlist_btn = translate(self.original_text_rename_playlist_btn,
                                                                 'en',
                                                                 self.languages[language])

            self.translated_text_delete_playlist_btn = translate(self.original_text_delete_playlist_btn,
                                                                 'en',
                                                                 self.languages[language])

            self.translated_text_info_playlists = translate(self.original_text_info_playlists,
                                                            'en',
                                                            self.languages[language])

            self.translated_text_choose_track = translate(self.original_text_choose_track,
                                                          'en',
                                                          self.languages[language])

            self.translated_text_choose_track = translate(self.original_text_choose_track,
                                                          'en',
                                                          self.languages[language])

            self.translated_text_enter_playlist_name = translate(self.original_text_enter_playlist_name,
                                                                 'en',
                                                                 self.languages[language])

            self.translated_text_choose_directory = translate(self.original_text_choose_directory,
                                                              'en',
                                                              self.languages[language])

            self.translated_text_error = translate(self.original_text_error,
                                                   'en',
                                                   self.languages[language])

            self.translated_text_error_name = translate(self.original_text_error_name,
                                                        'en',
                                                        self.languages[language])

            self.translated_text_error_exists = translate(self.original_text_error_exists,
                                                          'en',
                                                          self.languages[language])

            # Русская адаптания
            if self.languages[language] == 'ru':
                self.ui.create_playlist_button.setGeometry(225, 330, 151, 23)
                self.ui.text_sample_rate.setGeometry(460, 90, 131, 16)
                self.ui.sample_rate.setGeometry(505, 110, 41, 21)
                self.ui.text_track_volume.setGeometry(480, 140, 101, 16)
                self.ui.track_volume.setGeometry(515, 160, 31, 21)
                self.ui.text_channel_count.setGeometry(470, 190, 111, 16)
                self.ui.channel_count.setGeometry(520, 210, 31, 21)
                self.ui.text_playlists.setGeometry(155, 50, 101, 21)
            elif self.languages[language] == 'uk':
                self.ui.create_playlist_button.setGeometry(225, 330, 191, 23)
                self.ui.text_sample_rate.setGeometry(460, 90, 131, 16)
                self.ui.sample_rate.setGeometry(505, 110, 41, 21)
                self.ui.text_track_volume.setGeometry(480, 140, 101, 16)
                self.ui.track_volume.setGeometry(510, 160, 31, 21)
                self.ui.text_channel_count.setGeometry(475, 190, 111, 16)
                self.ui.channel_count.setGeometry(515, 210, 31, 21)
                self.ui.text_playlists.setGeometry(155, 50, 101, 21)
            elif self.languages[language] == 'be':
                self.ui.create_playlist_button.setGeometry(225, 330, 151, 23)
                self.ui.text_sample_rate.setGeometry(460, 90, 131, 16)
                self.ui.sample_rate.setGeometry(505, 110, 41, 21)
                self.ui.text_track_volume.setGeometry(480, 140, 101, 16)
                self.ui.track_volume.setGeometry(515, 160, 31, 21)
                self.ui.text_channel_count.setGeometry(470, 190, 111, 16)
                self.ui.channel_count.setGeometry(520, 210, 31, 21)
                self.ui.text_playlists.setGeometry(155, 50, 101, 21)
            elif self.languages[language] == 'en':
                self.ui.create_playlist_button.setGeometry(245, 330, 111, 23)
                self.ui.text_sample_rate.setGeometry(506, 90, 101, 16)
                self.ui.text_sample_rate.setFont(QFont("MS Shell Dlg 2", 10))
                self.ui.sample_rate.setGeometry(525, 110, 41, 21)
                self.ui.text_track_volume.setGeometry(504, 140, 101, 16)
                self.ui.text_track_volume.setFont(QFont("MS Shell Dlg 2", 10))
                self.ui.track_volume.setGeometry(535, 160, 31, 21)
                self.ui.text_channel_count.setGeometry(490, 190, 111, 16)
                self.ui.text_channel_count.setFont(QFont("MS Shell Dlg 2", 10))
                self.ui.channel_count.setGeometry(537, 210, 31, 21)
                self.ui.text_playlists.setGeometry(165, 50, 101, 21)
            elif self.languages[language] == 'es':
                # TO-DO
                pass
                # self.ui.text_sample_rate.setGeometry(460, 90, 131, 16)
                # self.ui.sample_rate.setGeometry(505, 110, 41, 21)
                # self.ui.text_track_volume.setGeometry(480, 140, 101, 16)
                # self.ui.track_volume.setGeometry(515, 160, 31, 21)
                # self.ui.text_channel_count.setGeometry(470, 190, 111, 16)
                # self.ui.channel_count.setGeometry(520, 210, 31, 21)
                # self.ui.text_playlists.setGeometry(155, 50, 101, 21)

            #
            self.ui.text_playlist_name.setFont(QFont("MS Shell Dlg 2", 11, QFont.Bold))

            self.translated_playlists_title = translate(self.original_playlists_title,
                                                        'en',
                                                        self.languages[language])

            self.play_text_translated = translate(self.play_text,
                                                  'en',
                                                  self.languages[language])

            self.pause_text_translated = translate(self.pause_text,
                                                   'en',
                                                   self.languages[language])

            if self.btn_play_status == "Pause":
                self.ui.play_pause_button.setText(self.pause_text_translated)
            else:
                self.ui.play_pause_button.setText(self.play_text_translated)

            self.translated_player_title = translate(self.original_player_title,
                                                     'en',
                                                     self.languages[language])

            self.setWindowTitle(self.translated_player_title)

        # Перевод окна плейлистов
        counter = 0

        for element, (key, value) in zip(self.playlist_window_elements_lst, self.english_names_playlists.items()):
            if counter > 4:
                break
            elif value != "":
                translated_text = translate(value,
                                            'en',
                                            self.languages[language])
                if translated_text is None:
                    break
                else:
                    element.setText(translated_text)
                    counter += 1

/**
*    \defgroup AVCP_module The AVCP_module
*    \brief This is a player module that normalizes the tracks being played.
*
*    Auto volume control player class and C functions for working with ctypes in Python.
*    Supports the "Windows" operating system starting with "Windows 7".
*/
///@{


#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "windows.h"
#include "audiere.h"
#include "Playlist.h"
typedef class AudioStreamInfo AudioStreamInfo;

/**
*    Structure with information about track
*/

struct AudioStreamInfo {
	char *title = NULL;
	char *path = NULL;
	char *playlist = NULL;
	int channel_count = 0;
	int sample_rate = 0;
	int block_size = 0;
	float volume = 0.0f;
	bool is_playing = false;
	bool is_paused = false;
	bool is_processing = false;
};


/**
*    \brief Player class for normalizing the player
*    \author Melnikov Matvey
*    \version 0.2
*    \date May 18, 2020
*
*    The player calculates the RMS power and normalizes the track volume
*/


class AutoVolumeControlPlayer
{
public:
	AutoVolumeControlPlayer();
	~AutoVolumeControlPlayer();
	std::wstring getPath(); ///< Path to the folder with tracks
	void setPath(std::wstring dir); ///< Set the path to the folder with tracks
	void play(); ///< Play track
	void pause(); ///< Pause track
	void stop(); ///< Stop player
	void playNext(); ///< Play next track
	void playPrevious(); ///< Play previous track
	float getTrackTime(); ///< Get the duration of the current track
	float getTrackTimeNow(); ///< Get the duration of the current track now
	void setTrackTimeNow(int seconds); ///< Sets the position of the track
	std::vector <std::string> getQueue(); ///< Get a track queue
	AudioStreamInfo getTrackInfo(); ///< Get information about current track
	void startPlaylist(std::string playlist_name); ///< Start playing a playlist
	void createPlaylist(std::string playlist_name); ///< Create a playlist
	void deletePlaylist(std::string playlist_name); ///< Delete a playlist
	void renamePlaylist(std::string new_playlist_name, std::string playlist_name); ///< Rename a playlist
	void addTrackToPlaylist(std::string track_path, std::string playlist_name); ///< Adding a track to a playlist
	void deleteTrackFromPlaylist(std::string track_name, std::string playlist_name); ///< Deleting a track from a playlist
	void loadTracksFromFile(); ///< Loading tracks from a file
	void savePlaylists(); ///< Saving tracks in a file
	bool findPlaylist(std::string playlist_name); ///< Find a playlist
	std::vector <std::string> getPlaylists(); ///< Get a list of playlists
	bool findTrack(std::string track_name, std::string playlist_name); ///< Find a track in a playlist
	std::vector <std::string> getTracks(std::string playlist_name); ///< Get a list of tracks in a playlist
private:
	float getPowerRms(std::string trackname);
	std::vector <std::string> getTracks(std::wstring wdir);
	static void readSource(std::string trackname, short *buf, int start_position, int size);
	void getAudioData(std::string trackname, short *sample_data);
	void collectInfo(float volume);
	void findSuitableTrack(bool next_track);
	static void threadFunction(AutoVolumeControlPlayer * player);
private:
	int ratio = 1500;
	std::wstring wdir = L"none";
	int current_pos = 0;
	std::vector <std::string> tracks_queue;
	audiere::SampleSource* source = NULL;
	audiere::AudioDevicePtr device = NULL;
	bool is_playing = false;
	bool interrupt = false;
	bool is_pause = false;
	bool processing = false;
	bool control = false;
	std::string track_title = "None";
	std::string track_path = "None";
	std::string track_playlist = "None";
	AudioStreamInfo info;
	PlaylistsStore playlists;
};


#ifdef __cplusplus
extern "C" {
#endif

	AutoVolumeControlPlayer *getNew();
	void deleteClass(AutoVolumeControlPlayer *_class);
	char *AVCP_getPath(AutoVolumeControlPlayer *_class);
	void AVCP_setPath(AutoVolumeControlPlayer *_class, char *dir);
	void AVCP_play(AutoVolumeControlPlayer *_class);
	void AVCP_pause(AutoVolumeControlPlayer *_class);
	void AVCP_stop(AutoVolumeControlPlayer *_class);
	void AVCP_playNext(AutoVolumeControlPlayer *_class);
	void AVCP_playPrevious(AutoVolumeControlPlayer *_class);
	float AVCP_getTrackTime(AutoVolumeControlPlayer *_class);
	float AVCP_getTrackTimeNow(AutoVolumeControlPlayer *_class);
	void AVCP_setTrackTimeNow(AutoVolumeControlPlayer *_class, int seconds);
	char **AVCP_getQueue(AutoVolumeControlPlayer *_class, int *size);
	AudioStreamInfo AVCP_getTrackInfo(AutoVolumeControlPlayer *_class);

	void AVCP_startPlaylist(AutoVolumeControlPlayer *_class, char *playlist_name);
	void AVCP_createPlaylist(AutoVolumeControlPlayer *_class, char *playlist_name);
	void AVCP_deletePlaylist(AutoVolumeControlPlayer *_class, char *playlist_name);
	void AVCP_renamePlaylist(AutoVolumeControlPlayer *_class, char *new_playlist_name, char *playlist_name);
	void AVCP_addTrackToPlaylist(AutoVolumeControlPlayer *_class, char *track_path, char *playlist_name);
	void AVCP_deleteTrackFromPlaylist(AutoVolumeControlPlayer *_class, char *track_name, char *playlist_name);
	void AVCP_loadTracksFromFile(AutoVolumeControlPlayer *_class);
	void AVCP_savePlaylists(AutoVolumeControlPlayer *_class);
	bool AVCP_findPlaylist(AutoVolumeControlPlayer *_class, char *playlist_name);
	char **AVCP_getPlaylists(AutoVolumeControlPlayer *_class, int *size);
	bool AVCP_findTrack(AutoVolumeControlPlayer *_class, char *track_name, char *playlist_name);
	char **AVCP_getTracks(AutoVolumeControlPlayer *_class, char *playlist_name, int *size);

#ifdef __cplusplus
}
#endif


///@}


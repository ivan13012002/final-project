﻿/**
*    \file
*    \brief File with description of class methods and binding from python code.
*
*    Description of auto volume control class methods and C functions for working with ctypes in Python.
*    Supports the "Windows" operating system starting with " Windows 7".
*/

#define _CRT_SECURE_NO_WARNINGS

#include "windows.h"
#include "mmdeviceapi.h"
#include "audioclient.h"
#include <endpointvolume.h>
#include <Psapi.h>
#include <iostream>
#include "mingw.thread.h"
#include <stdbool.h>
#include <vector>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>
#include <cmath>
#include "AutoVolumeControl.h"

#define EXIT_ON_ERROR(hres)  \
              if (FAILED(hres)) { goto Exit; }

#define SAFE_RELEASE(punk)  \
              if ((punk) != NULL)  \
                { (punk)->Release(); (punk) = NULL; }

#define REFTIMES_PER_SEC  10000000
#define REFTIMES_PER_MILLISEC  10000

const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
const IID IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);
const IID IID_IAudioClient = __uuidof(IAudioClient);
const IID IID_IAudioCaptureClient = __uuidof(IAudioCaptureClient);


AutoVolumeControl::AutoVolumeControl()
{
	default_volume = 0.3f;
	current_volume = default_volume;
	range = 0.1f;
	min_volume_border = default_volume - range;
	max_volume_border = default_volume + range;
	ratio = default_volume * 0.4;
	step = range / 100;
	SetMasterVolume(current_volume);
	CoInitialize(NULL);
	loadSpecifiedVolume();
	std::thread process_check_thread(checkForegroundProcessName, this);
	process_check_thread.detach();
}


AutoVolumeControl::~AutoVolumeControl()
{
	is_working_process_thread = false;
	stopVolumeControl();
}


void AutoVolumeControl::startVolumeControl()
{
	if (is_working) return;
	is_working = true;
	std::thread thread(threadFunction, this);
	thread.detach();
}


void AutoVolumeControl::stopVolumeControl()
{
	is_working = false;
	Sleep(100);
}


bool AutoVolumeControl::isWorking()
{
	return is_working;
}


/**
*    Method for finding out the volume of the system mixer.
*/

float AutoVolumeControl::getMasterVolume()
{
	// Позволяет узнать громкость микшера
	HRESULT hr;
	IMMDeviceEnumerator *pEnumerator = NULL;
	IMMDevice *pDevice = NULL;
	IAudioEndpointVolume *g_pEndptVol = NULL;
	LPCGUID guid = NULL;
	float master_volume = 0;

	hr = CoCreateInstance(__uuidof(MMDeviceEnumerator),
		NULL, CLSCTX_INPROC_SERVER,
		__uuidof(IMMDeviceEnumerator),
		(void**)&pEnumerator);
	if (FAILED(hr)) master_volume = -1;
	EXIT_ON_ERROR(hr)

		hr = pEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &pDevice);
	if (FAILED(hr)) master_volume = -1;
	EXIT_ON_ERROR(hr)

		hr = pDevice->Activate(__uuidof(IAudioEndpointVolume),
			CLSCTX_ALL, NULL, (void**)&g_pEndptVol);
	if (FAILED(hr)) master_volume = -1;
	EXIT_ON_ERROR(hr)

		g_pEndptVol->GetMasterVolumeLevelScalar(&master_volume);

Exit:
	SAFE_RELEASE(pEnumerator)
		SAFE_RELEASE(pDevice)
		SAFE_RELEASE(g_pEndptVol)
		return master_volume;
}


/**
*    Returns the volume value that is set while the app is running,
*    but the volume value is not saved for it.
*/

float AutoVolumeControl::getDefaultVolume()
{
	return default_volume;
}


/**
*    Sets the default volume.
*    \param[in] _default_volume The volume to be set. For example: 0.2 (20%).
*/

void AutoVolumeControl::setDefaultVolume(float _default_volume)
{
	default_volume = _default_volume;
	if (default_volume > 1.0f) default_volume = 1.0f;
	else if (default_volume < 0.0f) default_volume = 0.0f;
	min_volume_border = default_volume - range;
	max_volume_border = default_volume + range;
	ratio = default_volume * 0.4;
	step = range / 100;
}


/**
*    Returns the current volume value.
*/

float AutoVolumeControl::getVolume()
{
	return current_volume * 100;
}


/**
*    Returns the range of volume.
*/

float AutoVolumeControl::getRange()
{
	return range;
}


/**
*    Sets the range of volume.
*    \param[in] _range The range to be set. For example: 0.2 (20%).
*/

void AutoVolumeControl::setRange(float _range)
{
	range = _range;
	if (range > 1.0f) range = 1.0f;
	else if (range < 0.0f) range = 0.0f;
	min_volume_border = default_volume - range;
	max_volume_border = default_volume + range;
	ratio = current_volume * 0.4;
	step = range / 100;
}


/**
*    Saves the app and the volume for it in a file (name of file: "info").
*    \param[in] app_name Name of the app that will be saved. For example: any_app.exe.
*    \param[in] _volume The volume that will be saved for this app. For example: 0.2 (20%).
*/

void AutoVolumeControl::saveSpecifiedVolume(std::string app_name, float _volume)
{
	// Сохраняем название приложения и его громкость
	if (app_name.empty()) return;
	std::ofstream file("info", std::ios_base::app);
	if (file.is_open())
	{
		int volume = 0;
		if (_volume > 1.0f) volume = 100;
		else if (_volume < 0.0f) volume = 0;
		else volume = _volume * 100;
		file << app_name + " " + std::to_string(volume) + ";\n";
	}
	file.close();
	loadSpecifiedVolume();
}


/**
*    Deletes the saved volume value for the app by position.
*    \param[in] pos_of_program The position of app, which will be deleted.
*/

void AutoVolumeControl::deleteSpecifiedVolume(int pos_of_program)
{
	if ((pos_of_program < 0) or (pos_of_program > loaded_programs.size()))
		return;
	auto it = loaded_programs.begin();
	for (int i = 0; i < pos_of_program; i++)
		it++;
	loaded_programs.erase(it);

	// Удаляем сохраненные данные
	std::ofstream clearfile("info", std::ofstream::out | std::ios::trunc);
	clearfile.close();

	std::ofstream file("info", std::ios_base::app);
	if (file.is_open())
	{
		for (auto it = loaded_programs.begin(); it != loaded_programs.end(); it++)
			file << (*it).first + " " + std::to_string((*it).second) + ";\n";
	}
	file.close();
	loadSpecifiedVolume();
}


/**
*    Deletes all saved volume values.
*/

void AutoVolumeControl::clearSavedVolumes()
{
	// Удаляем сохраненные данные
	std::ofstream file("info", std::ofstream::out | std::ios::trunc);
	file.close();
	loaded_programs.clear();
}


/**
*    Returns a list of programs with saved volume values from a file (name of file: "info").
*/

std::vector<std::string> AutoVolumeControl::getSpecifiedVolumeApps()
{
	// Возвращаем список программ, с сохраненным значением громкости
	loadSpecifiedVolume();
	std::vector<std::string> specified_volume_apps;
	for (auto it = loaded_programs.begin(); it != loaded_programs.end(); it++)
		specified_volume_apps.push_back((*it).first + " " + std::to_string((*it).second));
	return specified_volume_apps;
}


/**
*    Returns a list of programs used by the user.
*/

std::vector<std::string> AutoVolumeControl::getLatestApps()
{
	return latest_programs;
}

float AutoVolumeControl::getFloat(BYTE _byte1, BYTE _byte2, BYTE _byte3, BYTE _byte4)
{
	//float IEE754 - from dword to float
	DWORD dword;
	((BYTE*)&dword)[0] = _byte1;
	((BYTE*)&dword)[1] = _byte2;
	((BYTE*)&dword)[2] = _byte3;
	((BYTE*)&dword)[3] = _byte4;
	return *reinterpret_cast<float*>(&dword);
}


float AutoVolumeControl::setVolumeChange(float current_volume, float next_volume, float step)
{
	// Implements a smooth volume change
	if (abs(current_volume - next_volume) <= step) // If the change meets the step, then change the volume
	{
		return next_volume;
	}
	else if ((abs(current_volume - next_volume) >= step * 70) and (next_volume < current_volume))
	{
		return calculateVolume(current_volume, next_volume, step, 30);
	}
	else if (abs(current_volume - next_volume) >= step * 30)
	{
		return calculateVolume(current_volume, next_volume, step, 10);
	}
	else if (abs(current_volume - next_volume) >= step * 10)
	{
		return calculateVolume(current_volume, next_volume, step, 1);
	}
	else
		return current_volume;
}


float AutoVolumeControl::getNewVolume(float power_rms, float last_power_rms)
{
	// Method that handles exceptions
	if (power_rms == 0) // If it is silent, then the maximum volume
	{
		return max_volume_border;
	}
	if (((power_rms - last_power_rms) >= 0.006) and last_power_rms != 0) // If the difference between capacities is large
		// Then we lower the volume to the minimum level
	{
		return float(min_volume_border);
	}
	else if (setVolumeChange(
		getMasterVolume(), (float((ratio / power_rms) / 100)), step) < min_volume_border)
	{
		return float(min_volume_border); // If the volume is out of borders
	}
	else if (setVolumeChange(
		getMasterVolume(), (float((ratio / power_rms) / 100)), step) > max_volume_border)
	{
		return float(max_volume_border); // If the volume is out of borders
	}
	else // If no exceptions are found, we return the volume with a smooth change
	{
		return setVolumeChange(
			getMasterVolume(), float((ratio / power_rms) / 100), step);
	}
	return -1.0f;
}


float AutoVolumeControl::calculateVolume(float current_volume, float next_volume, float step, float coefficient)
{
	// Method that calculates the next volume in increments
	float new_volume = 0;
	if (current_volume > next_volume)
		new_volume = current_volume - step * coefficient;
	else
		new_volume = current_volume + step * coefficient;
	if (new_volume > max_volume_border)
		return max_volume_border;
	else if (new_volume < min_volume_border)
		return min_volume_border;
	else
		return new_volume;
}


HRESULT AutoVolumeControl::SetMasterVolume(float volume)
{
	// Allows you to set the volume of the mixer
	HRESULT result = 1;
	HRESULT hr;
	IMMDeviceEnumerator *pEnumerator = NULL;
	IMMDevice *pDevice = NULL;
	IAudioEndpointVolume *g_pEndptVol = NULL;
	LPCGUID guid = NULL;

	hr = CoCreateInstance(__uuidof(MMDeviceEnumerator),
		NULL, CLSCTX_INPROC_SERVER,
		__uuidof(IMMDeviceEnumerator),
		(void**)&pEnumerator);
	if (FAILED(hr)) result = -1;
	EXIT_ON_ERROR(hr)

		hr = pEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &pDevice);
	if (FAILED(hr)) result = -1;
	EXIT_ON_ERROR(hr)

		hr = pDevice->Activate(__uuidof(IAudioEndpointVolume),
			CLSCTX_ALL, NULL, (void**)&g_pEndptVol);
	if (FAILED(hr)) result = -1;
	EXIT_ON_ERROR(hr)

		if (volume > 1.0f) volume = 1.0f;
		else if (volume < 0.0f) volume = 0.0f;

	hr = g_pEndptVol->SetMasterVolumeLevelScalar(volume, guid);
	if (FAILED(hr)) result = -1;

Exit:
	SAFE_RELEASE(pEnumerator)
		SAFE_RELEASE(pDevice)
		SAFE_RELEASE(g_pEndptVol)
		return result;
}


void AutoVolumeControl::setVolume(float _current_volume)
{
	current_volume = _current_volume;
	if (current_volume > 1.0f) current_volume = 1.0f;
	else if (current_volume < 0.0f) current_volume = 0.0f;
	min_volume_border = current_volume - range;
	max_volume_border = current_volume + range;
	ratio = default_volume * 0.4;
	step = range / 100;
}


void AutoVolumeControl::threadFunction(AutoVolumeControl *controlclass)
{
	// Method intercepting the stream and adjusting the volume
	CoInitialize(NULL);
	HRESULT hr;
	REFERENCE_TIME hnsRequestedDuration = REFTIMES_PER_SEC;
	REFERENCE_TIME hnsActualDuration;
	UINT32 bufferFrameCount;
	UINT32 numFramesAvailable = 1;
	IMMDeviceEnumerator *pEnumerator = NULL;
	IMMDevice *pDevice = NULL;
	IAudioClient *pAudioClient = NULL;
	IAudioCaptureClient *pCaptureClient = NULL;
	WAVEFORMATEX *pwfx = NULL;
	UINT32 packetLength = 0;
	BOOL bDone = FALSE;
	BYTE *pData;
	DWORD flags;
	float last_RMS = 0;

	hr = CoCreateInstance(
		CLSID_MMDeviceEnumerator, NULL,
		CLSCTX_ALL, IID_IMMDeviceEnumerator,
		(void**)&pEnumerator);
	EXIT_ON_ERROR(hr)

		hr = pEnumerator->GetDefaultAudioEndpoint(
			eRender, eConsole, &pDevice);
	EXIT_ON_ERROR(hr)

		hr = pDevice->Activate(
			IID_IAudioClient, CLSCTX_ALL,
			NULL, (void**)&pAudioClient);
	EXIT_ON_ERROR(hr)

		// Getting the audio stream format
		hr = pAudioClient->GetMixFormat(&pwfx);
	EXIT_ON_ERROR(hr)

		// Initialization of the client
		hr = pAudioClient->Initialize(
			AUDCLNT_SHAREMODE_SHARED,
			AUDCLNT_STREAMFLAGS_LOOPBACK,
			hnsRequestedDuration,
			0,
			pwfx,
			NULL);
	EXIT_ON_ERROR(hr)

		// Getting the buffer size
		hr = pAudioClient->GetBufferSize(&bufferFrameCount);
	EXIT_ON_ERROR(hr)

		hr = pAudioClient->GetService(
			IID_IAudioCaptureClient,
			(void**)&pCaptureClient);
	EXIT_ON_ERROR(hr)

		hnsActualDuration = (double)REFTIMES_PER_SEC *
		bufferFrameCount / pwfx->nSamplesPerSec;

	hr = pAudioClient->Start();  // The start of the capture
	EXIT_ON_ERROR(hr)

		while (controlclass->is_working)
		{
			int amount_of_frequency = 0;
			int amount_of_RMS = 0;
			float power_RMS = 0;

			Sleep(hnsActualDuration / REFTIMES_PER_MILLISEC / 2);

			hr = pCaptureClient->GetNextPacketSize(&packetLength);
			EXIT_ON_ERROR(hr)

				amount_of_RMS = packetLength;

			while (packetLength != 0)
			{
				float frequency = 0;
				double frequency_sum = 0;
				double sqrt_frequency_sum = 0;
				amount_of_frequency = (numFramesAvailable * 2);
				// Reading audio data to the buffer
				hr = pCaptureClient->GetBuffer(
					&pData,
					&numFramesAvailable,
					&flags, NULL, NULL);
				EXIT_ON_ERROR(hr)

					if (flags & AUDCLNT_BUFFERFLAGS_SILENT)
					{
						pData = NULL;
					}

				if (pData != NULL)
					for (int i = 0; i < (numFramesAvailable * 8) - 3; i += 4) // numFramesAvailable * block size (nBlockAlign)
					{
						frequency =
							controlclass->getFloat(pData[i], pData[i + 1], pData[i + 2], pData[i + 3]); // Getting a float from 4 bytes

						frequency_sum += frequency; // The amount of samples
						sqrt_frequency_sum += pow(frequency, 2); // The sum of the squares of samples
					}

				power_RMS +=
					sqrt((sqrt_frequency_sum - (pow(frequency_sum, 2) / amount_of_frequency)) / (amount_of_frequency - 1));
				// Counting RMS power

				hr = pCaptureClient->ReleaseBuffer(numFramesAvailable);
				EXIT_ON_ERROR(hr)

					hr = pCaptureClient->GetNextPacketSize(&packetLength);
				EXIT_ON_ERROR(hr)
			}

			if (amount_of_RMS != NULL)
			{
				controlclass->SetMasterVolume(controlclass->getNewVolume((power_RMS / amount_of_RMS), last_RMS));
				last_RMS = power_RMS / amount_of_RMS;
			}
			else
			{
				controlclass->
					SetMasterVolume(
						controlclass->
						setVolumeChange(controlclass->getMasterVolume(), controlclass->max_volume_border, controlclass->step));
				last_RMS = 0;
			}
		}
	hr = pAudioClient->Stop();  // The end of the capture
	EXIT_ON_ERROR(hr)

		Exit:
	CoTaskMemFree(pwfx);
	SAFE_RELEASE(pEnumerator)
		SAFE_RELEASE(pDevice)
		SAFE_RELEASE(pAudioClient)
		SAFE_RELEASE(pCaptureClient)
		CoUninitialize();
}


std::string AutoVolumeControl::getForegroundProcessName()
{
	// Getting the name of the active window process
	HWND win;
	win = GetForegroundWindow();
	DWORD dwPID = 0;
	GetWindowThreadProcessId(win, &dwPID);

	const HANDLE hProc = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, dwPID);
	if (win)
	{
		if (hProc)
		{
			TCHAR path[MAX_PATH];
			GetProcessImageFileName(hProc, path, sizeof(path));
			CloseHandle(hProc);

			TCHAR title[MAX_PATH];
			if (GetFileTitle(path, title, sizeof(title)) == 0)
			{
				std::string stitle = "";
#ifndef UNICODE
				stitle = title;
#else
				std::wstring wtitle = title;
				stitle = std::string(wtitle.begin(), wtitle.end());
#endif
				return stitle;
			}
			else
			{
				std::string spath = "";
#ifndef UNICODE
				spath = path;
#else
				std::wstring wpath = path;
				spath = std::string(wpath.begin(), wpath.end());
#endif
				return spath;
			}
		}
		else
			return std::string("error");
	}
	else
		return std::string("error");
}


void AutoVolumeControl::checkForegroundProcessName(AutoVolumeControl *controlclass)
{
	// Checking the active window process
	std::string program_name = "";
	while (controlclass->is_working_process_thread)
	{
		program_name = controlclass->getForegroundProcessName();
		if (program_name != "error")
		{
			if (controlclass->latest_programs.empty())
			{
				controlclass->latest_programs.push_back(program_name);
			}
			else if (program_name != controlclass->latest_programs.back())
			{
				if (controlclass->latest_programs.size() == 8)
					controlclass->latest_programs.erase(controlclass->latest_programs.begin());

				if (std::find(controlclass->latest_programs.begin(), controlclass->latest_programs.end(), program_name)
					== controlclass->latest_programs.end())
				{
					controlclass->latest_programs.push_back(program_name);
				}

				if (controlclass->loaded_programs.find(program_name) != controlclass->loaded_programs.end() and
					controlclass->current_volume != (*controlclass->loaded_programs.find(program_name)).second)
				{
					// If you have a volume saved for this app, then set it
					controlclass->setVolume(float((*controlclass->loaded_programs.find(program_name)).second) / 100.0f);
				}
				else // Otherwise, we set the default volume
					controlclass->setVolume(controlclass->default_volume);
			}
		}
	}
}


void AutoVolumeControl::loadSpecifiedVolume()
{
	// Loading the saved volume values and app names from the file
	loaded_programs.clear();
	std::ifstream file("info", std::ios_base::in);
	if (file.is_open())
	{
		while (file)
		{
			std::string programm_str = "";
			std::string volume_str = "";
			std::getline(file, programm_str);
			if (!programm_str.empty())
			{
				while (!programm_str.empty())
				{
					if (isdigit(programm_str.back()))
						volume_str.push_back(programm_str.back());
					else if (programm_str.back() == ' ')
						break;
					programm_str.pop_back();
				}
				programm_str.pop_back();
				if (!programm_str.empty() and !(volume_str.empty()))
				{
					std::reverse(volume_str.begin(), volume_str.end());
					int volume = atoi(volume_str.c_str());
					if (volume > 100) volume = 100;
					else if (volume < 0) volume = 0;

					loaded_programs[programm_str] = volume;
				}
			}
		}
	}
	file.close();
}


//////////////////////////////
//Helper function


char** vectorToCharPtr(std::vector<std::string> _vector, int *size)
{
	char** charptr = new char*[_vector.size()];
	for (int i = 0; i < _vector.size(); i++)
	{
		charptr[i] = new char[_vector[i].size() + 1];
		strcpy(charptr[i], _vector[i].c_str());
	}
	*size = _vector.size();
	return charptr;
}


//////////Binding C++ code
//Constructor and destructor


AutoVolumeControl *GetNew()
{
	return new AutoVolumeControl();
}


void DeleteClass(AutoVolumeControl *_class)
{
	delete _class;
}


//Methods


void AVC_startVolumeControl(AutoVolumeControl *_class)
{
	_class->startVolumeControl();
}


void AVC_stopVolumeControl(AutoVolumeControl *_class)
{
	_class->stopVolumeControl();
}


bool AVC_isWorking(AutoVolumeControl *_class)
{
	return _class->isWorking();
}


float AVC_getMasterVolume(AutoVolumeControl *_class)
{
	return _class->getMasterVolume();
}


float AVC_getDefaultVolume(AutoVolumeControl *_class)
{
	return _class->getDefaultVolume();
}


void AVC_setDefaultVolume(AutoVolumeControl *_class, float _default_vol)
{
	_class->setDefaultVolume(_default_vol);
}


float AVC_getVolume(AutoVolumeControl *_class)
{
	return _class->getVolume();
}


float AVC_getRange(AutoVolumeControl *_class)
{
	return _class->getRange();
}


void AVC_setRange(AutoVolumeControl *_class, float _range)
{
	_class->setRange(_range);
}


void AVC_saveSpecifiedVolume(AutoVolumeControl *_class, char *_app_name, float _volume)
{
	std::string app_name(_app_name);
	_class->saveSpecifiedVolume(app_name, _volume);
}

void AVC_deleteSpecifiedVolume(AutoVolumeControl *_class, int pos_of_program)
{
	_class->deleteSpecifiedVolume(pos_of_program);
}


void AVC_clearSavedVolumes(AutoVolumeControl *_class)
{
	_class->clearSavedVolumes();
}


char **AVC_getSpecifiedVolumeApps(AutoVolumeControl *_class, int *size)
{
	return vectorToCharPtr(_class->getSpecifiedVolumeApps(), size);
}


char **AVC_getLatestApps(AutoVolumeControl *_class, int *size)
{
	return vectorToCharPtr(_class->getLatestApps(), size);
}


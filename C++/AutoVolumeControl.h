/**
*    \defgroup AVC_module The AVC_module
*    \brief This module is designed to adjust the volume of the system
*
*    Auto volume control class and C functions for working with ctypes in Python.
*    Supports the "Windows" operating system starting with " Windows 7".
*    @{
*/

#include <iostream>
#include "mmdeviceapi.h"
#include <endpointvolume.h>
#include "audioclient.h"
#include "windows.h"
#include <vector>
#include <map>


/**
*    \brief Class for adjusting the volume of the system
*    \author Melnikov Matvey
*	 \version 0.1
*    \date April 20, 2020
*
*    This class intercepts the audio stream of the mixer and sets the volume level
*        in the specified range based on the received audio data
*/

class AutoVolumeControl
{
public:
	AutoVolumeControl();
	~AutoVolumeControl();
	void startVolumeControl(); ///< Start volume control
	void stopVolumeControl(); ///< Stop volume control
	bool isWorking();
	float getMasterVolume(); ///< Get the system volume
	float getDefaultVolume(); ///< Get the default volume
	void setDefaultVolume(float _default_volume); ///< Set the default volume
	float getVolume(); ///< Get the current volume
	float getRange(); ///< Get the range
	void setRange(float _range); ///< Set the volume range
	void saveSpecifiedVolume(std::string app_name, float _volume); ///< To save the volume setting for the app
	void deleteSpecifiedVolume(int pos_of_program);
	void clearSavedVolumes(); ///< To clear the stored values
	std::vector<std::string> getSpecifiedVolumeApps(); ///< To obtain the name of the application with the stored loudness values
	std::vector<std::string> getLatestApps(); ///< Get the last 8 programs used by the user
private:
	float getFloat(BYTE _byte1, BYTE _byte2, BYTE _byte3, BYTE _byte4);
	float setVolumeChange(float current_volume, float next_volume, float step);
	float getNewVolume(float total_range, float last_range);
	float calculateVolume(float current_volume, float next_volume, float step, float coefficient);
	HRESULT SetMasterVolume(float volume);
	void setVolume(float _current_volume);
	static void threadFunction(AutoVolumeControl *controlclass);
	std::string getForegroundProcessName();
	static void checkForegroundProcessName(AutoVolumeControl *controlclass);
	void loadSpecifiedVolume();
private:
	float default_volume = 0.0f;
	float current_volume = 0.0f;
	float range = 0.0f;
	float min_volume_border = 0.0f;
	float max_volume_border = 0.0f;
	float ratio = 0.0f;
	float step = 0.0f;
	bool is_working = false;
	bool is_working_process_thread = true;
	std::vector<std::string> latest_programs;
	std::map <std::string, int> loaded_programs;
};

#ifdef __cplusplus
extern "C" {
#endif

	AutoVolumeControl *GetNew(); ///< Class constructor
	void DeleteClass(AutoVolumeControl *_class); ///< Class destructor

	void AVC_startVolumeControl(AutoVolumeControl *_class);
	void AVC_stopVolumeControl(AutoVolumeControl *_class);
	bool AVC_isWorking(AutoVolumeControl *_class);
	float AVC_getMasterVolume(AutoVolumeControl *_class);
	float AVC_getDefaultVolume(AutoVolumeControl *_class);
	void AVC_setDefaultVolume(AutoVolumeControl *_class, float _default_vol);
	float AVC_getVolume(AutoVolumeControl *_class);
	float AVC_getRange(AutoVolumeControl *_class);
	void AVC_setRange(AutoVolumeControl *_class, float _range);
	void AVC_saveSpecifiedVolume(AutoVolumeControl *_class, char *_app_name, float _volume);
	void AVC_deleteSpecifiedVolume(AutoVolumeControl *_class, int pos_of_program);
	void AVC_clearSavedVolumes(AutoVolumeControl *_class);
	char **AVC_getSpecifiedVolumeApps(AutoVolumeControl *_class, int *size);
	char **AVC_getLatestApps(AutoVolumeControl *_class, int *size);

#ifdef __cplusplus
}
#endif

/**
*    @}
*/

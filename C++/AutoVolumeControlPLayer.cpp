﻿/**
*    \addtogroup AVCP_module
*/
///@{
/**
*    \file
*    \brief File with description of methods of the auto volume control player class and C wrapper of C++ code.
*
*    Description of auto volume control player class methods and C functions for working with ctypes in Python.
*    Supports the "Windows" operating system starting with " Windows 7".
*/


#include <iostream>
#include <windows.h>
#include <string.h>
#include <vector>
#include <cmath>
#include "mingw.thread.h"
#include "audiere.h"
#include "AutoVolumeControlPlayer.h"
#include "Playlist.h"
#define _CRT_SECURE_NO_WARNINGS


/////////////////Public members

AutoVolumeControlPlayer::AutoVolumeControlPlayer()
{
	device = audiere::OpenDevice(NULL, NULL); // Opening the default playback device
	playlists.loadTracksFromFile();
	info =
	{
		const_cast<char *>(track_title.c_str()), const_cast<char *>(track_path.c_str()),
		const_cast<char *>(track_playlist.c_str()), 0, 0, 0, 0.0f, false, false, false
	};
}

AutoVolumeControlPlayer::~AutoVolumeControlPlayer()
{
	stop();
}


/**
*    Returns the path to the folder with tracks.
*/

std::wstring AutoVolumeControlPlayer::getPath()
{
	return wdir;
}


/**
*    Sets the path to the folder with tracks.
*    \param[in] dir The path to the tracks. For example: D:\\\music\\\.
*/

void AutoVolumeControlPlayer::setPath(std::wstring dir)
{
	// Sets the path to the folder with tracks
	if (dir.back() != '\\')
		dir.push_back('\\');
	if (dir.back() != '*')
		dir.push_back('*');

	WIN32_FIND_DATAW wfd;
	HANDLE const hFind = FindFirstFileW(LPCWSTR(dir.c_str()), &wfd);
	if (INVALID_HANDLE_VALUE != hFind)
	{
		stop();
		wdir = dir;
	}
	else if (wdir == L"none") // If the path is wrong
	{
		std::cerr << "Wrong path\n";
		exit(-1);
	}
	tracks_queue = getTracks(wdir);
	track_playlist = "None";
}


/**
*    Starts playing tracks.
*/

void AutoVolumeControlPlayer::play()
{
	if (!is_playing)
	{
		processing = true;
		info.is_processing = true;
		current_pos = 0;
		is_playing = true;
		std::thread player_thread(threadFunction, this); // Creating a stream for playing a track
		player_thread.detach(); // Disconnecting the thread from the main thread
	}
	else
	{
		is_pause = false;
		info.is_paused = false;
	}
}


/**
*    Pauses the playback of tracks.
*/

void AutoVolumeControlPlayer::pause()
{
	info.is_paused = true;
	is_pause = true;
}


/**
*    Sets all default flags and stops all threads.
*/


void AutoVolumeControlPlayer::stop()
{
	is_playing = false;
	is_pause = false;
	interrupt = false;
	current_pos = 0;
	track_title = "None";
	track_path = "None";
	info =
	{
		const_cast<char *>(track_title.c_str()), const_cast<char *>(track_path.c_str()),
		const_cast<char *>(track_playlist.c_str()), 0, 0, 0, 0.0f, false, false, false
	};
	Sleep(100);
}


void AutoVolumeControlPlayer::playNext()
{
	if (processing) return;
	processing = true;
	info.is_processing = true;
	interrupt = true;
	control = true;
	is_pause = false;
	current_pos++;
	if ((size_t)current_pos >= tracks_queue.size())
		current_pos = 0;
	findSuitableTrack(true);
}


void AutoVolumeControlPlayer::playPrevious()
{
	if (processing) return;
	processing = true;
	info.is_processing = true;
	interrupt = true;
	control = true;
	is_pause = false;
	current_pos--;
	if (current_pos < 0)
		current_pos = tracks_queue.size() - 1;
	findSuitableTrack(false);
}


/**
*    Returns the total track time in seconds.
*/

float AutoVolumeControlPlayer::getTrackTime()
{
	// Returns the total track time in seconds
	if (!source)
		return 0;
	audiere::SampleFormat format;
	int rate = 0;
	int number_of_channels = 0;
	source->getFormat(number_of_channels, rate, format); // Reading information about the format
	float track_time = float(source->getLength() / float(60 * rate)); // Calculating the time in minutes
	return track_time * 60;
}


/**
*    Returns the current track time in seconds (as a decimal).
*/

float AutoVolumeControlPlayer::getTrackTimeNow()
{
	// Returns the current track time in seconds
	if (!source) return 0;
	audiere::SampleFormat format;
	int rate = 0;
	int number_of_channels = 0;
	source->getFormat(number_of_channels, rate, format); // Reading information about the format
	float time_now = float(source->getPosition() / float(60 * rate)); // Calculating the time in minutes
	return time_now * 60;
}


/**
*    Sets the track position in seconds.
*    \param[in] seconds Position in seconds. For example: 60.
*/

void AutoVolumeControlPlayer::setTrackTimeNow(int seconds)
{
	// float time_dec = float(seconds) / 60.0f; // Convert to minutes
	if (seconds > getTrackTime())
		return;
	audiere::SampleFormat format;
	int rate = 0;
	int number_of_channels = 0;
	source->getFormat(number_of_channels, rate, format); // Reading information about the format
	int pos = seconds * rate;
	source->setPosition(pos);
}

/**
*    Returns the tracks that are in the queue.
*/

std::vector <std::string> AutoVolumeControlPlayer::getQueue()
{
	std::vector <std::string> queue = tracks_queue;
	for (auto it = queue.begin(); it != queue.end(); it++)
	{
		auto it2 = it->rfind('\\');
		if (it2 != it->npos)
			it->erase(0, it2 + 1);
	}
	return queue;
}

/**
*    Returns information about the track.
*/

AudioStreamInfo AutoVolumeControlPlayer::getTrackInfo()
{
	return info;
}

/**
*    Returns information about the track.
*    \param[in] playlist_name Name of the playlist to launch. For example: Classic.
*/

void AutoVolumeControlPlayer::startPlaylist(std::string playlist_name)
{
	if (!playlists.findPlaylist(playlist_name))
		return;
	stop();
	tracks_queue = playlists.getTracks(playlist_name);
	track_playlist = playlist_name;
	play();
}

/**
*    Creates an empty playlist and saves it to a file (name of file: "playlists").
*    \param[in] playlist_name Name of the playlist to create. For example: Classic.
*/

void AutoVolumeControlPlayer::createPlaylist(std::string playlist_name)
{
	playlists.createPlaylist(playlist_name);
}

/**
*    Deletes a playlist from a file (name of file: "playlists").
*    \param[in] playlist_name Name of the playlist to delete. For example: Classic.
*/

void AutoVolumeControlPlayer::deletePlaylist(std::string playlist_name)
{
	playlists.deletePlaylist(playlist_name);
}

/**
*    Renames the playlist and saves it to a file (name of file: "playlists").
*    \param[in] new_playlist_name New file name. For example: Jazz.
*    \param[in] playlist_name The current file name. For example: Classic.
*/

void AutoVolumeControlPlayer::renamePlaylist(std::string new_playlist_name, std::string playlist_name)
{
	playlists.renamePlaylist(new_playlist_name, playlist_name);
}

/**
*    Adds a track to the playlist.
*    \param[in] track_path Path to the track to be added. For example: D:\Music\Anymusic.mp3.
*    \param[in] playlist_name The playlist that the track will be added to. For example: Classic.
*/

void AutoVolumeControlPlayer::addTrackToPlaylist(std::string track_path, std::string playlist_name)
{
	playlists.addTrackToPlaylist(track_path, playlist_name);
}

/**
*    Deletes a track from the playlist.
*    \param[in] track_name Name of the track to delete. For example: Anymusic.mp3.
*    \param[in] playlist_name Name of the playlist that the track will be deleted from. For example: Classic.
*/

void AutoVolumeControlPlayer::deleteTrackFromPlaylist(std::string track_name, std::string playlist_name)
{
	playlists.deleteTrackFromPlaylist(track_name, playlist_name);
}

/**
*    Loads tracks and playlists from a file (name of file: "playlists").
*/

void AutoVolumeControlPlayer::loadTracksFromFile()
{
	playlists.loadTracksFromFile();
}

/**
*    Saves tracks and playlists to a file (name of file: "playlists").
*/

void AutoVolumeControlPlayer::savePlaylists()
{
	playlists.savePlaylists();
}

/**
*    Checks whether the playlist exists.
*    \param[in] playlist_name Name of the playlist to search for. For example: Classic.
*/

bool AutoVolumeControlPlayer::findPlaylist(std::string playlist_name)
{
	return playlists.findPlaylist(playlist_name);
}

/**
*    Returns a list of all saved playlists.
*/

std::vector <std::string> AutoVolumeControlPlayer::getPlaylists()
{
	return playlists.getPlaylists();
}

/**
*    Checks whether the track exists.
*    \param[in] track_name Name of the track to search for. For example: Anymusic.mp3.
*    \param[in] playlist_name Name of the playlist to search in. For example: Classic.
*/

bool AutoVolumeControlPlayer::findTrack(std::string track_name, std::string playlist_name)
{
	return playlists.findTrack(track_name, playlist_name);
}

/**
*    Returns a list of tracks in the playlist.
*    \param[in] playlist_name The playlist that tracks will be received from. For example: Classic.
*/

std::vector <std::string> AutoVolumeControlPlayer::getTracks(std::string playlist_name)
{
	return playlists.getTracks(playlist_name);
}

/////////////////Private members

float AutoVolumeControlPlayer::getPowerRms(std::string trackname)
{
	// Method that counts the rms power of the track
	int size = source->getLength() / 2;
	short int *sample_data = new short int[size]; // Creating a buffer for audio data

	getAudioData(trackname, sample_data);
	unsigned int sqrt_sum = 0;
	int sum = 0;
	float power_rms = 0.0f;
	for (int i = 0; i < size; i++)
	{
		sqrt_sum += (int)pow(sample_data[i], 2);
		sum += sample_data[i];
		if (i % 4 == 0 and i != 0) // 4 * 32768 * 32768 (short size) = 4294967296‬ (~unsigned int size)
		{
			// RMS = sqrt((sqrt_sum - ((sum ^ 2) / N)) / (N - 1)) - formula
			// sum - sum of sample; sqrt_sum - sum of sample squares; N - number of samples
			float new_power_rms = sqrt(float(sqrt_sum - (pow(sum, 2) / 4.0f)) / 3.0f);
			if (power_rms == 0)
				power_rms = new_power_rms;
			else if (sum != 0)
				power_rms = (power_rms + new_power_rms) / 2.0f; // average of rms
			sqrt_sum = 0;
			sum = 0;
		}
	}
	delete[] sample_data;
	processing = false;
	return power_rms;
}

std::vector <std::string> AutoVolumeControlPlayer::getTracks(std::wstring wdir)
{
	// Method that finds all tracks in a folder with a valid format
	WIN32_FIND_DATAW wfd;
	HANDLE const hFind = FindFirstFileW(LPCWSTR(wdir.c_str()), &wfd);
	std::vector <std::string> folders;
	std::string dir = std::string(wdir.begin(), wdir.end());
	dir.pop_back();

	if (INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			std::wstring wfile_name(&wfd.cFileName[0]);
			std::string file_name(wfile_name.begin(), wfile_name.end());
			if ((file_name.find(".mp3") != std::string::npos) or (file_name.find(".wav") != std::string::npos) or
				(file_name.find(".ogg") != std::string::npos)) // Supported formats
				folders.push_back(dir + file_name);
		} while (NULL != FindNextFileW(hFind, &wfd));

		FindClose(hFind);
	}
	else // If the path is wrong
	{
		std::cerr << "Wrong path\n";
		exit(-1);
	}

	return folders;
}

void AutoVolumeControlPlayer::readSource(std::string trackname, short *_buf, int start_position, int size)
{
	// Stream for reading audio data
	int *buf = new int[size * 2];
	audiere::SampleSource* source = audiere::OpenSampleSource(trackname.c_str());
	source->ref();
	source->read(size, buf);
	source->reset();
	source->unref();
	for (int i = 0; i < size / 2; i++)
	{
		// Reading every second sample
		_buf[i] = short((float(buf[i * 2]) / 2147483647.0f) * 32767); // Get short from int
	}
	delete[] buf;
}

void AutoVolumeControlPlayer::getAudioData(std::string trackname, short *sample_data)
{
	// A method that reads audio data in streams and collects it into a single buffer
	int size = source->getLength();
	if (size == 0) return;
	short int *sample_data_1 = new short int[size / 4]; // Create the buffers for reading in the threads
	short int *sample_data_2 = new short int[size / 4];
	for (int i = 0; i < size / 4; i++)
	{
		sample_data_1[i] = 0;
		sample_data_2[i] = 0;
	}

	// Starting threads and getting 1/4 of data to each buffer
	std::thread first_read_source(
		readSource, trackname, sample_data_1, 0, size / 4);
	std::thread second_read_source(
		readSource, trackname, sample_data_2, size / 2, size / 4);
	// Returning threads
	if (first_read_source.joinable())
		first_read_source.join();
	if (second_read_source.joinable())
		second_read_source.join();

	// Combining data into a single buffer
	for (int i = 0; i < size / 4; i++)
	{
		sample_data[i] = sample_data_1[i];
	}
	delete[] sample_data_1;
	for (int i = 0; i < size / 4; i++)
	{
		sample_data[i + (size / 4)] = sample_data_2[i];
	}
	delete[] sample_data_2;
}

void AutoVolumeControlPlayer::collectInfo(float volume)
{
	// Get information about the track
	audiere::SampleFormat sample_format;
	int channel_count = 0, sample_rate = 0, block_size = 0;
	source->getFormat(channel_count, sample_rate, sample_format);
	if (sample_format == audiere::SF_S16)
		block_size = 16;
	else
		block_size = 8;

	std::string title = tracks_queue[current_pos];
	auto it = title.rfind('\\');
	if (it != title.npos)
		title.erase(0, it + 1);
	track_title = title;
	track_path = tracks_queue[current_pos];
	info =
	{
		const_cast<char *>(track_title.c_str()), const_cast<char *>(track_path.c_str()),
		const_cast<char *>(track_playlist.c_str()), channel_count, sample_rate,
		block_size, volume, is_playing, is_pause, false
	};
}

void AutoVolumeControlPlayer::findSuitableTrack(bool next_track)
{
	// Open files until we find a working one  
	if (source or tracks_queue.empty())
		return;
	while (true)
	{
		if ((size_t)current_pos >= tracks_queue.size())
			current_pos = 0;
		else if ((size_t)current_pos < 0)
			current_pos = tracks_queue.size() - 1;
		source = audiere::OpenSampleSource((tracks_queue[current_pos]).c_str());
		if (source)
		{
			if (source->getLength() > 0)
				break;
		}
		if (next_track)
			current_pos++;
		else
			current_pos--;
	}
}

void AutoVolumeControlPlayer::threadFunction(AutoVolumeControlPlayer * player)
{
	// The thread in which tracks are played
	while (player->is_playing and player->tracks_queue.size() != 0)
	{
		player->findSuitableTrack(true);
		player->source->ref();
		float power_rms = player->getPowerRms(player->tracks_queue[player->current_pos]);

		player->source->reset();
		audiere::OutputStreamPtr sound = OpenSound(player->device, player->source, true);
		if (sound)
		{
			sound->setVolume(0.5);
			float new_volume = player->ratio / power_rms;
			if (new_volume >= 1)
				sound->setVolume(1.0f);
			else if (new_volume <= 0)
				sound->setVolume(0.0f);
			else
				sound->setVolume(new_volume);

			// Get information about the track
			player->collectInfo(sound->getVolume());

			sound->setPosition(1500);
			sound->play();
			while (sound->isPlaying())
			{
				// Processing user commands
				player->device->update();
				if (player->interrupt)
				{
					sound->stop();
					player->interrupt = false;
					break;
				}
				else if (player->is_pause)
				{
					sound->stop();
					while (player->is_pause) {}
					sound->play();
				}
				if (!player->is_playing)
				{
					sound->stop();
					break;
				}
			}
		}
		if (player->source)
		{
			// Clearing source
			player->source->unref();
			player->source = nullptr;
		}
		Sleep(50);
		if (!player->interrupt and !player->is_pause and player->is_playing and !player->control)
		{
			player->playNext();
		}
		player->control = false;
		player->processing = false;
		player->interrupt = false;
	}
}

//////////Binding C with the code of class methods
//Constructor and destructor

AutoVolumeControlPlayer *getNew()
{
	return new AutoVolumeControlPlayer();
}

void deleteClass(AutoVolumeControlPlayer *_class)
{
	delete _class;
}

//Methods

char *AVCP_getPath(AutoVolumeControlPlayer *_class)
{
	std::wstring wstr = _class->getPath();
	char *str = new char[wstr.length() + 1];
	std::string sstr(wstr.begin(), wstr.end());
	strcpy(str, sstr.c_str());
	return str;
}

void AVCP_setPath(AutoVolumeControlPlayer *_class, char *dir)
{
	std::string sstr(dir);
	std::wstring wstr(sstr.begin(), sstr.end());
	_class->setPath(wstr);
}

void AVCP_play(AutoVolumeControlPlayer *_class)
{
	_class->play();
}

void AVCP_pause(AutoVolumeControlPlayer *_class)
{
	_class->pause();
}

void AVCP_stop(AutoVolumeControlPlayer *_class)
{
	_class->stop();
}

void AVCP_playNext(AutoVolumeControlPlayer *_class)
{
	_class->playNext();
}

void AVCP_playPrevious(AutoVolumeControlPlayer *_class)
{
	_class->playPrevious();
}

float AVCP_getTrackTime(AutoVolumeControlPlayer *_class)
{
	return _class->getTrackTime();
}

float AVCP_getTrackTimeNow(AutoVolumeControlPlayer *_class)
{
	return _class->getTrackTimeNow();
}

void AVCP_setTrackTimeNow(AutoVolumeControlPlayer *_class, int seconds)
{
	_class->setTrackTimeNow(seconds);
}

char **AVCP_getQueue(AutoVolumeControlPlayer *_class, int *size)
{
	std::vector <std::string> vqueue = _class->getQueue();
	char **queue = new char*[vqueue.size()];
	for (int i = 0; i < vqueue.size(); i++)
	{
		queue[i] = new char[vqueue[i].size() + 1];
		strcpy(queue[i], vqueue[i].c_str());
	}
	*size = vqueue.size();
	return queue;
}

AudioStreamInfo AVCP_getTrackInfo(AutoVolumeControlPlayer *_class)
{
	return _class->getTrackInfo();
}

//Playlist methods

void AVCP_startPlaylist(AutoVolumeControlPlayer *_class, char *playlist_name)
{
	_class->startPlaylist(std::string(playlist_name));
}

void AVCP_createPlaylist(AutoVolumeControlPlayer *_class, char *playlist_name)
{
	_class->createPlaylist(std::string(playlist_name));
}

void AVCP_deletePlaylist(AutoVolumeControlPlayer *_class, char *playlist_name)
{
	_class->deletePlaylist(std::string(playlist_name));
}

void AVCP_renamePlaylist(AutoVolumeControlPlayer *_class, char *new_playlist_name, char *playlist_name)
{
	_class->renamePlaylist(std::string(new_playlist_name), std::string(playlist_name));
}

void AVCP_addTrackToPlaylist(AutoVolumeControlPlayer *_class, char *track_path, char *playlist_name)
{
	_class->addTrackToPlaylist(std::string(track_path), std::string(playlist_name));
}

void AVCP_deleteTrackFromPlaylist(AutoVolumeControlPlayer *_class, char *track_name, char *playlist_name)
{
	_class->deleteTrackFromPlaylist(std::string(track_name), std::string(playlist_name));
}

void AVCP_loadTracksFromFile(AutoVolumeControlPlayer *_class)
{
	_class->loadTracksFromFile();
}

void AVCP_savePlaylists(AutoVolumeControlPlayer *_class)
{
	_class->savePlaylists();
}

bool AVCP_findPlaylist(AutoVolumeControlPlayer *_class, char *playlist_name)
{
	return _class->findPlaylist(std::string(playlist_name));
}

char **AVCP_getPlaylists(AutoVolumeControlPlayer *_class, int *size)
{
	std::vector <std::string> vplaylists = _class->getPlaylists();
	char **laylists = new char*[vplaylists.size()];
	for (int i = 0; i < vplaylists.size(); i++)
	{
		laylists[i] = new char[vplaylists[i].size() + 1];
		strcpy(laylists[i], vplaylists[i].c_str());
	}
	*size = vplaylists.size();
	return laylists;
}

bool AVCP_findTrack(AutoVolumeControlPlayer *_class, char *track_name, char *playlist_name)
{
	return _class->findTrack(std::string(track_name), std::string(playlist_name));
}

char **AVCP_getTracks(AutoVolumeControlPlayer *_class, char *playlist_name, int *size)
{
	std::vector <std::string> vtracks = _class->getTracks(std::string(playlist_name));
	char **tracks = new char*[vtracks.size()];
	for (int i = 0; i < vtracks.size(); i++)
	{
		tracks[i] = new char[vtracks[i].size() + 1];
		strcpy(tracks[i], vtracks[i].c_str());
	}
	*size = vtracks.size();
	return tracks;
}

///@}

"""Creates main menu window and processes actions in it."""

import requests
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QPalette, QColor, QFont

from main_menu_interface import Ui_MainMenuWindow
from player_window import PlayerWindow
from volume_control_window import AutoVolumeControlWindow


def translate(text, input_language, output_language):
    """Перевести текст с одного языка на другой."""
    token = "trnsl.1.1.20200525T174604Z.93ee72b36fca367e.60c2e208c749e9d38d60f2063306e261945096b5"
    url = "https://translate.yandex.net/api/v1.5/tr.json/translate"
    options = {'key': token,
               'lang': input_language + "-" + output_language,
               'text': text}

    try:
        request = requests.get(url, params=options, timeout=1.5).text
    except requests.exceptions.ConnectionError:
        request = None

    if request is None:
        return None
    else:
        return request[36:(len(request) - 3)]


class MainMenuWindow(QtWidgets.QMainWindow):
    """Class for main menu window processing."""

    def __init__(self):
        super(MainMenuWindow, self).__init__()
        self.ui = Ui_MainMenuWindow()
        self.ui.setupUi(self)
        self.setFixedSize(600, 408)
        self.setWindowTitle("Gridly")
        self.setWindowIcon(QIcon("./images/player_icon.jpg"))

        self.player_window = PlayerWindow()
        self.volume_control_window = AutoVolumeControlWindow()

        self.system_language = 'English'  # Стандартный язык программы

        self.languages = {'English': 'en',
                          'Русский': 'ru',
                          'Español': 'es',
                          'Український': 'uk',
                          'Беларускі': 'be'}

        self.palette = self.palette()
        self.palette.setColor(QPalette.Button, QColor(124, 175, 239))
        self.palette.setColor(QPalette.Highlight, QColor(124, 175, 239))
        self.setPalette(self.palette)

        # Элементы интерфейса окна главного меню (кроме кнопки "Back")
        self.main_menu_elements_lst = [self.ui.text_app_name,
                                       self.ui.text_app_version,
                                       self.ui.menu_btn_player,
                                       self.ui.menu_btn_vol_controller,
                                       self.ui.menu_btn_devs,
                                       self.ui.menu_btn_exit]

        # Текст на элементах плеера (на английском языке)
        self.english_names = {'text_app_name': 'Gridly',
                              'text_app_version': 'v. 1.0',
                              'menu_btn_player': 'Player',
                              'menu_btn_vol_controller': 'Volume controller',
                              'menu_btn_devs': 'Developers',
                              'menu_btn_exit': 'Exit',
                              'text_header_devs': "Project participants:"}

        # Элементы интерфейса раздела "Developers"
        self.devs_elements_lst = [self.ui.text_header_devs,
                                  self.ui.text_devs,
                                  self.ui.menu_btn_back]

        # Кнопки
        self.ui.menu_btn_player.clicked.connect(self.menu_btn_player_clicked)  # Кнопка "Player"
        self.ui.menu_btn_vol_controller.clicked.connect(self.menu_btn_vol_controller_clicked)
        self.ui.menu_btn_devs.clicked.connect(self.menu_btn_devs_clicked)  # Кнопка "Developers"
        self.ui.menu_btn_exit.clicked.connect(self.menu_btn_exit_clicked)  # Кнопка "Exit"

        self.ui.menu_btn_back.clicked.connect(self.menu_btn_back_clicked)  # Кнопка "Back"
        self.ui.menu_btn_back.hide()

        # Список языков
        self.ui.languages_list.activated[str].connect(self.language_selected)

        self.show_devs_elements(False)

    def __del__(self):
        pass

    def menu_btn_player_clicked(self):
        """Обработать кнопку 'Player'."""
        self.hide()
        self.player_window.show()

        if self.player_window.exec_():
            pass

        self.player_window.close()
        self.show()

    def menu_btn_vol_controller_clicked(self):
        """Обработать кнопку 'Volume controller'."""
        self.hide()
        self.volume_control_window.show()

        if self.volume_control_window.exec_():
            pass

        self.volume_control_window.close()
        self.show()

    def show_menu_elements(self, flag):
        """Показать/скрыть элементы страницы главного меню."""
        if flag is True:
            for element in self.main_menu_elements_lst:
                element.show()
        else:
            for element in self.main_menu_elements_lst:
                element.hide()

    def show_devs_elements(self, flag):
        """Показать/скрыть элементы подраздела 'Developers' главного меню."""
        if flag is True:
            for element in self.devs_elements_lst:
                element.show()
        else:
            for element in self.devs_elements_lst:
                element.hide()

    def menu_btn_devs_clicked(self):
        """Обработать кнопку 'Developers'."""
        self.show_menu_elements(False)
        self.show_devs_elements(True)

    def menu_btn_back_clicked(self):
        """Обработать кнопку 'Back'."""
        self.show_menu_elements(True)
        self.show_devs_elements(False)

    def menu_btn_exit_clicked(self):
        """Обработать кнопку 'Exit'."""
        self.close()

    def translate_interface(self, language):
        """Перевести интерфейс с одного языка на другой."""
        translated_text = None

        for element, (key, value) in zip(self.main_menu_elements_lst, self.english_names.items()):
            if key != 'text_app_name' and key != 'text_app_version':
                translated_text = translate(value,
                                            'en',
                                            self.languages[language])
                if translated_text is None:
                    break
                else:
                    element.setText(translated_text)

        translated_text = translate(self.english_names['text_header_devs'],
                                    'en',
                                    self.languages[language])

        if translated_text is not None:
            if self.languages[language] in ('ru', 'be', 'uk'):
                self.ui.text_header_devs.setText(translated_text)
                self.ui.text_header_devs.setFont(QFont("Segoe UI Emoji", 19, QFont.Bold))
                self.ui.text_header_devs.setAlignment(Qt.AlignCenter)
            elif self.languages[language] == 'en':
                self.ui.text_header_devs.setText(self.english_names['text_header_devs'])
                self.ui.text_header_devs.setFont(QFont("Segoe UI Emoji", 20, QFont.Bold))
                self.ui.text_header_devs.setAlignment(Qt.AlignCenter)

    def language_selected(self, language):
        """Обработать выбор языка из списка languages_list."""
        if self.system_language != language:
            self.translate_interface(language)
            self.player_window.translate_interface(language)
            self.volume_control_window.translate_interface(language)
            self.system_language = language

"""Tests for avc class."""
# -*- coding: utf-8 -*-

import unittest

from module_conversion import AVCClass


class TestAVCClass(unittest.TestCase):
    """Class for testing abc library."""
    def setUp(self):
        self.avc_object = AVCClass(0.3, 0.1)

    def tearDown(self):
        self.avc_object.clear_saved_volumes()

    def test_start_volume_control(self):
        self.avc_object.start_volume_control()
        self.assertTrue(self.avc_object.is_working())

    def test_stop_volume_control(self):
        self.avc_object.start_volume_control()
        self.avc_object.stop_volume_control()
        self.assertFalse(self.avc_object.is_working())

    def test_set_default_volume(self):
        self.avc_object.set_default_volume(0.4)
        self.assertEqual(self.avc_object.get_default_volume(), 0.4)
        self.avc_object.set_default_volume(40)
        self.assertEqual(self.avc_object.get_default_volume(), 1.0)
        self.avc_object.set_default_volume(-2412)
        self.assertEqual(self.avc_object.get_default_volume(), 0.0)

    def test_set_range(self):
        self.avc_object.set_range(1000)
        self.assertEqual(self.avc_object.get_range(), 1.0)
        self.avc_object.set_range(0.4)
        self.assertEqual(self.avc_object.get_range(), 0.4)
        self.avc_object.set_range(-213)
        self.assertEqual(self.avc_object.get_range(), 0.0)

    def test_get_latest_apps(self):
        self.avc_object.clear_saved_volumes()
        self.assertTrue(self.avc_object.get_latest_apps() == [])


if __name__ == '__main__':
    unittest.main()

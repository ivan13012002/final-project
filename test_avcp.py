"""Tests for avcp class."""
# -*- coding: utf-8 -*-

import unittest
import os
import time
from module_conversion_p import AVCPClass

class TestAVCPClass(unittest.TestCase):
    def setUp(self):
        self.avcp_object = AVCPClass()

    def test_init(self):
        self.assertEqual(self.avcp_object.get_path(), os.getcwd() + '\\*')

    def test_set_path(self):
        self.avcp_object.set_path('wqffqwfqwfq')
        self.assertEqual(self.avcp_object.get_path(), os.getcwd() + '\\*')
        self.avcp_object.set_path('D:\\')
        self.assertEqual(self.avcp_object.get_path(), 'D:\\*')

    def test_play(self):
        self.avcp_object.play()
        time.sleep(1.5)
        self.assertTrue(self.avcp_object.get_track_info()['is_playing'])
        self.assertNotEqual(self.avcp_object.get_track_info()['title'], 'None')
        self.avcp_object.stop()

    def test_pause(self):
        self.avcp_object.play()
        time.sleep(1.5)
        self.avcp_object.pause()
        self.assertTrue(self.avcp_object.get_track_info()['is_paused'])
        self.avcp_object.stop()

    def test_stop(self):
        self.avcp_object.stop()
        self.assertFalse(self.avcp_object.get_track_info()['is_playing'])
        self.assertEqual(self.avcp_object.get_track_info()['title'], 'None')

    def test_get_time(self):
        self.avcp_object.play()
        time.sleep(1.5)
        self.assertNotEqual(self.avcp_object.get_time(), 0)
        self.avcp_object.stop()

    def test_get_time_now(self):
        self.avcp_object.play()
        time.sleep(1.6)
        self.assertNotEqual(self.avcp_object.get_time_now(), 0)
        self.avcp_object.stop()

    def test_set_time_now(self):
        self.avcp_object.play()
        time.sleep(1.5)
        self.avcp_object.set_time_now(0)
        self.assertEqual(self.avcp_object.get_time_now(), 0)
        self.avcp_object.stop()

    def test_get_remaining_time(self):
        self.avcp_object.play()
        time.sleep(1.5)
        self.assertEqual(self.avcp_object.get_time() - self.avcp_object.get_time_now(),
                         self.avcp_object.get_remaining_time())
        self.avcp_object.stop()

    def test_create_playlist(self):
        self.avcp_object.create_playlist('Classic')
        self.assertTrue(self.avcp_object.find_playlist('Classic'))

    def test_delete_playlist(self):
        self.avcp_object.create_playlist('Classic')
        self.avcp_object.delete_playlist('Classic')
        self.assertFalse(self.avcp_object.find_playlist('Classic'))

    def test_rename_playlist(self):
        self.avcp_object.create_playlist('Classic')
        self.avcp_object.rename_playlist('Jazz', 'Classic')
        self.assertFalse(self.avcp_object.find_playlist('Classic'))
        self.assertTrue(self.avcp_object.find_playlist('Jazz'))

    def test_add_track_to_playlist(self):
        self.avcp_object.create_playlist('Classic')
        self.avcp_object.add_track_to_playlist('anytrack.mp3', 'Classic')
        self.assertTrue(self.avcp_object.find_track('anytrack.mp3', 'Classic'))

    def test_delete_track_from_playlist(self):
        self.avcp_object.create_playlist('Classic')
        self.avcp_object.add_track_to_playlist('anytrack.mp3', 'Classic')
        self.avcp_object.delete_track_from_playlist('anytrack.mp3', 'Classic')
        self.assertFalse(self.avcp_object.find_track('anytrack.mp3', 'Classic'))

    def test_get_tracks(self):
        self.avcp_object.create_playlist('Classic')
        self.avcp_object.add_track_to_playlist('anytrack.mp3', 'Classic')
        self.avcp_object.add_track_to_playlist('anytrack1.mp3', 'Classic')
        self.avcp_object.add_track_to_playlist('anytrack2.mp3', 'Classic')
        tracks = self.avcp_object.get_tracks('Classic')
        self.assertEqual(tracks[0], 'anytrack')
        self.assertEqual(tracks[1], 'anytrack1')
        self.assertEqual(tracks[2], 'anytrack2')

if __name__ == '__main__':
    unittest.main()

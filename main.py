"""Main file for running the application."""

import sys

from PyQt5 import QtWidgets

from main_menu_window import MainMenuWindow

app = QtWidgets.QApplication([])
app.setStyle('Fusion')

application = MainMenuWindow()
application.show()

sys.exit(app.exec_())
